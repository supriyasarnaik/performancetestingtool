package usercreation;

import java.sql.Timestamp;
import java.util.Objects;
public class User {

    private int userId;
    private String userFirstName;
    private String userLastName;
    private String userNumber;
    private String userVerifiedEmail;
    private String userEmail;
    private short userEmailIsVerified;
    private String userPassword;
    private String userAccessToken;
    private int userTerminalId;
    private int userTripId;
    private short userIsDriver;
    private short userIsLoggedIn;
    private long userEmailVerificationValidity;
    private String userDriverLoginId;
    private String userTractorNumber;
    private String userTrailerNumber;
    private String userHoSRule;
    private String userTz;
    private short userIsTzChanged;
    private int userDeviceId;
    private long userDevicePairedDate;
    private String userMsgJID;
    private int userExemptionId;
    private String userAppVersion;
    private long userLastFileSyncUpdate;
    private String dlName; //name as on driving license
    private String dlNum; //driving license number
    private int dlSt; //driving license - state of jurisdiction
    private short dlVerified;
    private String userShippingDocumentNumber;
    private Role role;
    private Company company;
    private short status;
    private int userClientId;
    private int userCreatedBy;
    private Timestamp userCreatedDt;
    private String userDOTNumber;
    private String userOANumber;

    public int getUserClientId() {
        return userClientId;
    }

    public void setUserClientId(int userClientId) {
        this.userClientId = userClientId;
    }

    public int getUserCreatedBy() {
        return userCreatedBy;
    }

    public void setUserCreatedBy(int userCreatedBy) {
        this.userCreatedBy = userCreatedBy;
    }

    public Timestamp getUserCreatedDt() {
        return userCreatedDt;
    }

    public void setUserCreatedDt(Timestamp userCreatedDt) {
        this.userCreatedDt = userCreatedDt;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getUserVerifiedEmail() {
        return userVerifiedEmail;
    }

    public void setUserVerifiedEmail(String userVerifiedEmail) {
        this.userVerifiedEmail = userVerifiedEmail;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public short getUserEmailIsVerified() {
        return userEmailIsVerified;
    }

    public void setUserEmailIsVerified(short userEmailIsVerified) {
        this.userEmailIsVerified = userEmailIsVerified;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserAccessToken() {
        return userAccessToken;
    }

    public void setUserAccessToken(String userAccessToken) {
        this.userAccessToken = userAccessToken;
    }

    public short getUserIsDriver() {
        return userIsDriver;
    }

    public void setUserIsDriver(short userIsDriver) {
        this.userIsDriver = userIsDriver;
    }

    public short getUserIsLoggedIn() {
        return userIsLoggedIn;
    }

    public int getUserTerminalId() {
        return userTerminalId;
    }

    public void setUserTerminalId(int userTerminalId) {
        this.userTerminalId = userTerminalId;
    }

    public int getUserTripId() {
        return userTripId;
    }

    public void setUserTripId(int userTripId) {
        this.userTripId = userTripId;
    }

    public void setUserIsLoggedIn(short userIsLoggedIn) {
        this.userIsLoggedIn = userIsLoggedIn;
    }

    public long getUserEmailVerificationValidity() {
        return userEmailVerificationValidity;
    }

    public void setUserEmailVerificationValidity(long userEmailVerificationValidity) {
        this.userEmailVerificationValidity = userEmailVerificationValidity;
    }

    public String getUserDriverLoginId() {
        return userDriverLoginId;
    }

    public void setUserDriverLoginId(String userDriverLoginId) {
        this.userDriverLoginId = userDriverLoginId;
    }

    public String getUserTractorNumber() {
        return userTractorNumber;
    }

    public void setUserTractorNumber(String userTractorNumber) {
        this.userTractorNumber = userTractorNumber;
    }

    public String getUserTrailerNumber() {
        return userTrailerNumber;
    }

    public void setUserTrailerNumber(String userTrailerNumber) {
        this.userTrailerNumber = userTrailerNumber;
    }

    public String getUserHoSRule() {
        return userHoSRule;
    }

    public void setUserHoSRule(String userHoSRule) {
        this.userHoSRule = userHoSRule;
    }

    public String getUserTz() {
        return userTz;
    }

    public void setUserTz(String userTz) {
        this.userTz = userTz;
    }

    public short getUserIsTzChanged() {
        return userIsTzChanged;
    }

    public void setUserIsTzChanged(short userIsTzChanged) {
        this.userIsTzChanged = userIsTzChanged;
    }
    
    public int getUserDeviceId() {
        return userDeviceId;
    }

    public void setUserDeviceId(int userDeviceId) {
        this.userDeviceId = userDeviceId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public long getUserDevicePairedDate() {
        return userDevicePairedDate;
    }

    public void setUserDevicePairedDate(long userDevicePairedDate) {
        this.userDevicePairedDate = userDevicePairedDate;
    }

    public String getUserMsgJID() {
        return userMsgJID;
    }

    public void setUserMsgJID(String userMsgJID) {
        this.userMsgJID = userMsgJID;
    }

    public int getUserExemptionId() {
        return userExemptionId;
    }

    public void setUserExemptionId(int userExemptionId) {
        this.userExemptionId = userExemptionId;
    }

    public String getUserAppVersion() {
        return userAppVersion;
    }

    public void setUserAppVersion(String userAppVersion) {
        this.userAppVersion = userAppVersion;
    }

    public String getDlName() {
        return dlName;
    }

    public void setDlName(String dlName) {
        this.dlName = dlName;
    }

    public String getDlNum() {
        return dlNum;
    }

    public void setDlNum(String dlNum) {
        this.dlNum = dlNum;
    }

    public int getDlSt() {
        return dlSt;
    }

    public void setDlSt(int dlSt) {
        this.dlSt = dlSt;
    }

    public short getDlVerified() {
        return dlVerified;
    }

    public void setDlVerified(short dlVerified) {
        this.dlVerified = dlVerified;
    }

    public long getUserLastFileSyncUpdate() {
        return userLastFileSyncUpdate;
    }

    public void setUserLastFileSyncUpdate(long userLastFileSyncUpdate) {
        this.userLastFileSyncUpdate = userLastFileSyncUpdate;
    }

    public String getUserShippingDocumentNumber() {
        return userShippingDocumentNumber;
    }

    public void setUserShippingDocumentNumber(String userShippingDocumentNumber) {
        this.userShippingDocumentNumber = userShippingDocumentNumber;
    }

    public String getUserDOTNumber() {
        return userDOTNumber;
    }

    public void setUserDOTNumber(String userDOTNumber) {
        this.userDOTNumber = userDOTNumber;
    }

    public String getUserOANumber() {
        return userOANumber;
    }

    public void setUserOANumber(String userOANumber) {
        this.userOANumber = userOANumber;
    }

    @Override
    public String toString() {
        return "User{"
                + "userId=" + userId
                + ", userFirstName=" + userFirstName
                + ", userLastName=" + userLastName
                + ", userNumber=" + userNumber
                + ", userVerifiedEmail=" + userVerifiedEmail
                + ", userEmail=" + userEmail
                + ", userEmailIsVerified=" + userEmailIsVerified
                + ", userPassword=" + userPassword
                + ", userAccessToken=" + userAccessToken
                + ", userTerminalId=" + userTerminalId
                + ", userIsDriver=" + userIsDriver
                + ", userIsLoggedIn=" + userIsLoggedIn
                + ", userEmailVerificationValidity=" + userEmailVerificationValidity
                + ", userDriverLoginId=" + userDriverLoginId
                + ", userTractorNumber=" + userTractorNumber
                + ", userTrailerNumber=" + userTrailerNumber
                + ", userHoSRule=" + userHoSRule
                + ", userDeviceId=" + userDeviceId
                + ", userDevicePairedDate=" + userDevicePairedDate
                + ", userMsgJID=" + userMsgJID
                + ", userExemptionId=" + userExemptionId
                + ", userAppVersion=" + userAppVersion
                + ", userLastFileSyncUpdate=" + userLastFileSyncUpdate
                + ", role=" + role.toString()
                + ", company=" + company.toString()
                + ", userOA=" + userOANumber
                + ", userDOT=" + userDOTNumber
                + '}';
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof User)) {
            return false;
        }
        User other = (User) obj;
        return this.userId == other.userId;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(userId);
    }

}
