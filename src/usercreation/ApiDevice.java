package usercreation;


public class ApiDevice {
    private String deviceSerialNumber;
    private String deviceIMEI;
    private String deviceBluetoothMac;
    private String deviceType;
    private String deviceWifiMac;
    private String deviceSimNumber;
    private int deviceMode;
    private String deviceIccId;
    private String deviceFirmwareVersion;

    public String getDeviceSerialNumber() {
        return deviceSerialNumber;
    }

    public void setDeviceSerialNumber(String deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }

    public String getDeviceIMEI() {
        return deviceIMEI;
    }

    public void setDeviceIMEI(String deviceIMEI) {
        this.deviceIMEI = deviceIMEI;
    }

    public String getDeviceBluetoothMac() {
        return deviceBluetoothMac;
    }

    public void setDeviceBluetoothMac(String deviceBluetoothMac) {
        this.deviceBluetoothMac = deviceBluetoothMac;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceWifiMac() {
        return deviceWifiMac;
    }

    public void setDeviceWifiMac(String deviceWifiMac) {
        this.deviceWifiMac = deviceWifiMac;
    }

    public String getDeviceSimNumber() {
        return deviceSimNumber;
    }

    public void setDeviceSimNumber(String deviceSimNumber) {
        this.deviceSimNumber = deviceSimNumber;
    }

    public int getDeviceMode() {
        return deviceMode;
    }

    public void setDeviceMode(int deviceMode) {
        this.deviceMode = deviceMode;
    }

    public String getDeviceIccId() {
        return deviceIccId;
    }

    public void setDeviceIccId(String deviceIccId) {
        this.deviceIccId = deviceIccId;
    }

    public String getDeviceFirmwareVersion() {
        return deviceFirmwareVersion;
    }

    public void setDeviceFirmwareVersion(String deviceFirmwareVersion) {
        this.deviceFirmwareVersion = deviceFirmwareVersion;
    }

    public void createApiDeviceObject(Device device) {
        this.setDeviceBluetoothMac(device.getDeviceBluetoothMacAddress());
        this.setDeviceWifiMac(device.getDeviceWifiMacAddress());
        this.setDeviceIMEI(device.getDeviceIMEI());
        //this.setDeviceMode(Integer.parseInt(device.getDeviceMode()));
        this.setDeviceSerialNumber(device.getDeviceSerialNumber());
        this.setDeviceSimNumber(device.getDeviceSimNumber());
        this.setDeviceIccId(device.getDeviceIccId());
        this.setDeviceType(device.getDeviceType());
        this.setDeviceFirmwareVersion(device.getDeviceFirmwareVersion());
    }

    @Override
    public String toString() {
        return "ApiDevice details [ " 
                + "deviceSerialNumber=" + deviceSerialNumber 
                + ", deviceIMEI=" + deviceIMEI
                + ", deviceBluetoothMac=" + deviceBluetoothMac 
                + ", deviceType=" + deviceType 
                + ", deviceWifiMac=" + deviceWifiMac 
                + ", deviceSimNumber=" + deviceSimNumber 
                + ", deviceMode=" + deviceMode 
                + ", deviceIccId=" + deviceIccId 
                + ", deviceFirmwareVersion=" + deviceFirmwareVersion + " ]";
    }

}
