 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usercreation;


public class EldEvents {

    private int id;
    private int serverId;
    private int clientId;
    private String driverLoginId;
    private int userId;
    private int recordOriginatorId;
    private int companyId;
    private String companyCode;
    private String deviceSerialNumber;
    private int hosId;
    private String vin;
    private String tractorNumber;
    private String trailerNumber;
    private String shippingDocNumber;
    private String sequenceId;
    private int type;
    private int code;
    private String eldDescription;
    private String dutyStatus;
    private String date;
    private String time;
    private long eventTs;
    private long eventFinishedTimestamp;
    private String dateTime;
    private long eventCreatedTs;
    private int utcOffset;
    private double vehicleMiles;
    private double engineHours;
    private String latitude;
    private String longitude;
    private double distance;
    private String malDiagCode;
    private int malIndicatorStatus;
    private int diagIndicatorStatus;
    private String comment;
    private String location;
    private String dataCheckValue;
    private int isChanged;
    private int recordOrigin;
    private int recordStatus;
    private int isActive;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getDriverLoginId() {
        return driverLoginId;
    }

    public void setDriverLoginId(String driverLoginId) {
        this.driverLoginId = driverLoginId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getRecordOriginatorId() {
        return recordOriginatorId;
    }

    public void setRecordOriginatorId(int recordOriginatorId) {
        this.recordOriginatorId = recordOriginatorId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getDeviceSerialNumber() {
        return deviceSerialNumber;
    }

    public void setDeviceSerialNumber(String deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }

    public int getHosId() {
        return hosId;
    }

    public void setHosId(int hosId) {
        this.hosId = hosId;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getTractorNumber() {
        return tractorNumber;
    }

    public void setTractorNumber(String tractorNumber) {
        this.tractorNumber = tractorNumber;
    }

    public String getTrailerNumber() {
        return trailerNumber;
    }

    public void setTrailerNumber(String trailerNumber) {
        this.trailerNumber = trailerNumber;
    }

    public String getShippingDocNumber() {
        return shippingDocNumber;
    }

    public void setShippingDocNumber(String shippingDocNumber) {
        this.shippingDocNumber = shippingDocNumber;
    }

    public String getSequenceId() {
        return sequenceId;
    }

    public void setSequenceId(String sequenceId) {
        this.sequenceId = sequenceId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getEldDescription() {
        return eldDescription;
    }

    public void setEldDescription(String eldDescription) {
        this.eldDescription = eldDescription;
    }

    public String getDutyStatus() {
        return dutyStatus;
    }

    public void setDutyStatus(String dutyStatus) {
        this.dutyStatus = dutyStatus;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public long getEventTs() {
        return eventTs;
    }

    public void setEventTs(long eventTs) {
        this.eventTs = eventTs;
    }

    public long getEventFinishedTimestamp() {
        return eventFinishedTimestamp;
    }

    public void setEventFinishedTimestamp(long eventFinishedTimestamp) {
        this.eventFinishedTimestamp = eventFinishedTimestamp;
    }

    
    
    
    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public long getEventCreatedTs() {
        return eventCreatedTs;
    }

    public void setEventCreatedTs(long eventCreatedTs) {
        this.eventCreatedTs = eventCreatedTs;
    }

    public int getUtcOffset() {
        return utcOffset;
    }

    public void setUtcOffset(int utcOffset) {
        this.utcOffset = utcOffset;
    }

    public double getVehicleMiles() {
        return vehicleMiles;
    }

    public void setVehicleMiles(double vehicleMiles) {
        this.vehicleMiles = vehicleMiles;
    }

    public double getEngineHours() {
        return engineHours;
    }

    public void setEngineHours(double engineHours) {
        this.engineHours = engineHours;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getMalDiagCode() {
        return malDiagCode;
    }

    public void setMalDiagCode(String malDiagCode) {
        this.malDiagCode = malDiagCode;
    }

    public int getMalIndicatorStatus() {
        return malIndicatorStatus;
    }

    public void setMalIndicatorStatus(int malIndicatorStatus) {
        this.malIndicatorStatus = malIndicatorStatus;
    }

    public int getDiagIndicatorStatus() {
        return diagIndicatorStatus;
    }

    public void setDiagIndicatorStatus(int diagIndicatorStatus) {
        this.diagIndicatorStatus = diagIndicatorStatus;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDataCheckValue() {
        return dataCheckValue;
    }

    public void setDataCheckValue(String dataCheckValue) {
        this.dataCheckValue = dataCheckValue;
    }

    public int getIsChanged() {
        return isChanged;
    }

    public void setIsChanged(int isChanged) {
        this.isChanged = isChanged;
    }

    public int getRecordOrigin() {
        return recordOrigin;
    }

    public void setRecordOrigin(int recordOrigin) {
        this.recordOrigin = recordOrigin;
    }

    public int getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(int recordStatus) {
        this.recordStatus = recordStatus;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    @Override
    public String toString() {
        return "EldEvents{" + "id=" + id + ", serverId=" + serverId + ", clientId=" + clientId + ", driverLoginId=" + driverLoginId + ", userId=" + userId + ", recordOriginatorId=" + recordOriginatorId + ", companyId=" + companyId + ", companyCode=" + companyCode + ", deviceSerialNumber=" + deviceSerialNumber + ", hosId=" + hosId + ", vin=" + vin + ", tractorNumber=" + tractorNumber + ", trailerNumber=" + trailerNumber + ", shippingDocNumber=" + shippingDocNumber + ", sequenceId=" + sequenceId + ", type=" + type + ", code=" + code + ", eldDescription=" + eldDescription + ", dutyStatus=" + dutyStatus + ", date=" + date + ", time=" + time + ", eventTs=" + eventTs + ", eventFinishedTimestamp=" + eventFinishedTimestamp + ", dateTime=" + dateTime + ", eventCreatedTs=" + eventCreatedTs + ", utcOffset=" + utcOffset + ", vehicleMiles=" + vehicleMiles + ", engineHours=" + engineHours + ", latitude=" + latitude + ", longitude=" + longitude + ", distance=" + distance + ", malDiagCode=" + malDiagCode + ", malIndicatorStatus=" + malIndicatorStatus + ", diagIndicatorStatus=" + diagIndicatorStatus + ", comment=" + comment + ", location=" + location + ", dataCheckValue=" + dataCheckValue + ", isChanged=" + isChanged + ", recordOrigin=" + recordOrigin + ", recordStatus=" + recordStatus + ", isActive=" + isActive + '}';
    }

 
}
