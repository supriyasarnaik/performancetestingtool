package usercreation;

import com.dc.ws.constants.ApplicationConstants.INTEGER_CONSTANTS;
import java.util.ArrayList;
import java.util.List;

public class ApiHoSV1 {

	private int serverHosLogId;

	private int clientLogId;

	private long createdDate;

	private short isLogCertified;

	private String certifiedBy;

	private int onDutyTime;

	private int drivingTime;

	private int offDutyTime;

	private int sleeperBerthTime;

	private long hosRuleUpdatedTimestamp;
        
        private String hosTz;

	private double tripDistance;

	//	private RuleSetInfo ruleSetInfo;

	private long ymCrossedTime;

	private long pcCrossedTime;

	private short isApproved;

	private String comment;

	private String editedBy;

	private String dot;

	private ArrayList<ApiHoSEventV1> status;

	private ArrayList<ApiHoSViolations> violation;

        
        private List<EldEvents> eldEvents;
        
        private short isActive;

	public int getServerHosLogId() {
		return serverHosLogId;
	}

	public void setServerHosLogId(int serverHosLogId) {
		this.serverHosLogId = serverHosLogId;
	}

	public int getClientLogId() {
		return clientLogId;
	}

	public void setClientLogId(int clientLogId) {
		this.clientLogId = clientLogId;
	}

	public long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}

	public short getIsLogCertified() {
		return isLogCertified;
	}

	public void setIsLogCertified(short isLogCertified) {
		this.isLogCertified = isLogCertified;
	}

	public String getCertifiedBy() {
		return certifiedBy;
	}

	public void setCertifiedBy(String certifiedBy) {
		this.certifiedBy = certifiedBy;
	}

	public int getOnDutyTime() {
		return onDutyTime;
	}

	public void setOnDutyTime(int onDutyTime) {
		this.onDutyTime = onDutyTime;
	}

	public int getDrivingTime() {
		return drivingTime;
	}

	public void setDrivingTime(int drivingTime) {
		this.drivingTime = drivingTime;
	}

	public int getOffDutyTime() {
		return offDutyTime;
	}

	public void setOffDutyTime(int offDutyTime) {
		this.offDutyTime = offDutyTime;
	}

	public int getSleeperBerthTime() {
		return sleeperBerthTime;
	}

	public void setSleeperBerthTime(int sleeperBerthTime) {
		this.sleeperBerthTime = sleeperBerthTime;
	}

	public long getHosRuleUpdatedTimestamp() {
		return hosRuleUpdatedTimestamp;
	}

	public void setHosRuleUpdatedTimestamp(long hosRuleUpdatedTimestamp) {
		this.hosRuleUpdatedTimestamp = hosRuleUpdatedTimestamp;
	}

        public String getHosTz() {
            return hosTz;
        }

        public void setHosTz(String hosTz) {
            this.hosTz = hosTz;
        }
    
	public double getTripDistance() {
		return tripDistance;
	}

	public void setTripDistance(double tripDistance) {
		this.tripDistance = tripDistance;
	}

	public long getYmCrossedTime() {
		return ymCrossedTime;
	}

	public void setYmCrossedTime(long ymCrossedTime) {
		this.ymCrossedTime = ymCrossedTime;
	}

	public long getPcCrossedTime() {
		return pcCrossedTime;
	}

	public void setPcCrossedTime(long pcCrossedTime) {
		this.pcCrossedTime = pcCrossedTime;
	}

	public ArrayList<ApiHoSEventV1> getStatus() {
		return status;
	}

	public void setStatus(ArrayList<ApiHoSEventV1> status) {
		this.status = (ArrayList<ApiHoSEventV1>) status;
	}

	public ArrayList<ApiHoSViolations> getViolation() {
		return violation;
	}

	public void setViolation(ArrayList<ApiHoSViolations> violation) {
		this.violation = (ArrayList<ApiHoSViolations>) violation;
	}

        public short getIsActive() {
            return isActive;
        }

        public void setIsActive(short isActive) {
            this.isActive = isActive;
        }

	public short getIsApproved() {
		return isApproved;
	}

	public void setIsApproved(short isApproved) {
		this.isApproved = isApproved;
	}

	public String getEditedBy() {
		return editedBy;
	}

	public void setEditedBy(String editedBy) {
		this.editedBy = editedBy;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public List<EldEvents> getEldEvents() {
		return eldEvents;
	}

	public void setEldEvents(List<EldEvents> eldEvents) {
		this.eldEvents = eldEvents;
	}

	public String getDot() {
		return dot;
	}

	public void setDot(String dot) {
		this.dot = dot;
	}

	public void createApiHoSObject(HoS hos, ArrayList<ApiHoSEventV1> apiHoSEvents, ArrayList<ApiHoSViolations> apiHoSViolations) {
		this.setServerHosLogId(hos.getHosId());
		this.setCreatedDate(hos.getHosCreatedDate());
		this.setOnDutyTime(hos.getHosOnDutyTime());
		this.setDrivingTime(hos.getHosDrivingTime());
		this.setOffDutyTime(hos.getHosOffDutyTime());
		this.setSleeperBerthTime(hos.getHosSleeperBerthTime());
		this.setHosRuleUpdatedTimestamp(hos.getHosRuleSetChangedDate());
                this.setHosTz(hos.getHosTimeZone());
		//this.setTripDistance(hos.getHosTripDistance());
		this.setYmCrossedTime(hos.getHosymCrossedTime());
		this.setPcCrossedTime(hos.getHosPCCrossedTime());
                short a = (short)0;
                if(hos.isStatus()) {
                    a = (short)1;
                }
                this.setIsActive(a);

		//set based on HoS value
		if (hos.isHosIsLogCertified()) {
			this.setIsLogCertified((short) 1);
			this.setCertifiedBy(hos.getHosCertifiedBy());
		} else {
			this.setIsLogCertified((short) 0);
		}
		this.setStatus(apiHoSEvents);
		this.setViolation(apiHoSViolations);
		this.setDot(hos.getHosDOTNumber());
	}



	@Override
	public String toString() {
		String apiHoSToStringResponse = "ApiHoS ["
				+ "serverHosLogId=" + serverHosLogId
				+ ", clientLogId=" + clientLogId
				+ ", createdDate=" + createdDate
				+ ", isLogCertified=" + isLogCertified
				+ ", certifiedBy=" + certifiedBy
				+ ", onDutyTime=" + onDutyTime
				+ ", drivingTime=" + drivingTime
				+ ", offDutyTime=" + offDutyTime
				+ ", sleeperBerthTime=" + sleeperBerthTime
				+ ", tripDistance=" + tripDistance
				+ ", dot="+dot
				+ ", status=" + status.size();
		if (status.size() > INTEGER_CONSTANTS.ZERO) {
			for (ApiHoSEventV1 statusObject : status) {
				apiHoSToStringResponse += statusObject.toString();
			}
		}
		apiHoSToStringResponse += ", violation=" + violation.size();
		if (violation.size() > INTEGER_CONSTANTS.ZERO) {
			for (ApiHoSViolations violationObject : violation) {
				apiHoSToStringResponse += violationObject.toString();
			}
		}
		apiHoSToStringResponse += ']';
		return apiHoSToStringResponse;
	}


}
