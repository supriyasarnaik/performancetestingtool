package usercreation;


public class RuleSetInfo
{

    public String userRuleSet;
    public int exemptionId;
    public int subStatusType;
    public short isAdverse;
    public short is100AirMile;

    public String getUserRuleSet() {
        return userRuleSet;
    }

    public void setUserRuleSet(String userRuleSet) {
        this.userRuleSet = userRuleSet;
    }

    public int getExemptionId() {
		return exemptionId;
	}

	public void setExemptionId(int exemptionId) {
		this.exemptionId = exemptionId;
	}

	public int getSubStatusType() {
        return subStatusType;
    }

    public void setSubStatusType(int subStatusType) {
        this.subStatusType = subStatusType;
    }


    public short getIsAdverse() {
        return isAdverse;
    }

    public void setIsAdverse(short isAdverse) {
        this.isAdverse = isAdverse;
    }

    public short getIs100AirMile() {
        return is100AirMile;
    }

    public void setIs100AirMile(short is100AirMile) {
        this.is100AirMile = is100AirMile;
    }


}
