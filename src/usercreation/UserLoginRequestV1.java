package usercreation;

public class UserLoginRequestV1 {

    private String driverId;

    private String password;

    private String companyCode;

    private short allowUserLogin;

    private long loginTime;

    public UserLoginRequestV1() {
        super();
    }

    public UserLoginRequestV1(String driverId, String password, String companyCode, short allowUserLogin) {
        super();
        this.driverId = driverId;
        this.password = password;
        this.companyCode = companyCode;
        this.allowUserLogin = allowUserLogin;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public short getAllowUserLogin() {
        return allowUserLogin;
    }

    public void setAllowUserLogin(short allowUserLogin) {
        this.allowUserLogin = allowUserLogin;
    }

    public long getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(long loginTime) {
        this.loginTime = loginTime;
    }

    @Override
    public String toString() {
        return "{" + "driverId=" + driverId + ", password=" + password + ", companyCode=" + companyCode + ", allowUserLogin=" + allowUserLogin + ", loginTime=" + loginTime + '}';
    }

}
