/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usercreation;

public class RegRequest {

    private String userAccessToken;

    private ApiDevice deviceDetails;

    private UserRegRequest userDetails;

    public RegRequest() {
        super();
    }

    public RegRequest(UserRegRequest userDetails) {
        super();
        this.userDetails = userDetails;
    }

    public RegRequest(String userAccessToken, ApiDevice deviceDetails) {
        super();
        this.userAccessToken = userAccessToken;
        this.deviceDetails = deviceDetails;
    }

    public RegRequest(String userAccessToken, ApiDevice deviceDetails, UserRegRequest userDetails) {
        super();
        this.userAccessToken = userAccessToken;
        this.deviceDetails = deviceDetails;
        this.userDetails = userDetails;
    }

    public String getUserAccessToken() {
        return userAccessToken;
    }

    public void setUserAccessToken(String userAccessToken) {
        this.userAccessToken = userAccessToken;
    }

    public ApiDevice getDeviceDetails() {
        return deviceDetails;
    }

    public void setDeviceDetails(ApiDevice deviceDetails) {
        this.deviceDetails = deviceDetails;
    }

    public UserRegRequest getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserRegRequest userDetails) {
        this.userDetails = userDetails;
    }

    @Override
    public String toString() {

        String toStringResponse = "Registration request details ";
        if (null != userDetails) {
            toStringResponse += "[ User details - " + userDetails.toString() + " ]";
        }
        if (null != deviceDetails) {
            toStringResponse += "[ Device details - " + deviceDetails.toString() + "; UserAccessToken - " + userAccessToken + " ]";
        }
        return toStringResponse;
    }

}
