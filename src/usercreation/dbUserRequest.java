/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usercreation;

/**
 *
 * @author ssarnaik
 */
public class dbUserRequest {
    
    private String userFirstName;
    private String userLastName;
    private String userNumber;
    private String userVerifiedEmail;
    private String userEmail;
    private int userEmailIsVerified;
    private String userPassword;
    private String userAccessToken;
    private int userTerminalId;
    private String userDriverLoginId;
    private String userHoSRule;
    private String userTz;
    private int role;
    private int company;
    private int userExemptionId;
    private long userEmailVerificationValidity;
    private int userIsDriver;
    private int userIsLoggedIn;

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getUserVerifiedEmail() {
        return userVerifiedEmail;
    }

    public void setUserVerifiedEmail(String userVerifiedEmail) {
        this.userVerifiedEmail = userVerifiedEmail;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public int getUserEmailIsVerified() {
        return userEmailIsVerified;
    }

public void setUserEmailIsVerified(int userEmailIsVerified) {
        this.userEmailIsVerified = userEmailIsVerified;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserAccessToken() {
        return userAccessToken;
    }

    public void setUserAccessToken(String userAccessToken) {
        this.userAccessToken = userAccessToken;
    }

    public int getUserTerminalId() {
        return userTerminalId;
    }

    public void setUserTerminalId(int userTerminalId) {
        this.userTerminalId = userTerminalId;
    }

    public String getUserDriverLoginId() {
        return userDriverLoginId;
    }

    public void setUserDriverLoginId(String userDriverLoginId) {
        this.userDriverLoginId = userDriverLoginId;
    }

    public String getUserHoSRule() {
        return userHoSRule;
    }

    public void setUserHoSRule(String userHoSRule) {
        this.userHoSRule = userHoSRule;
    }

    public String getUserTz() {
        return userTz;
    }

    public void setUserTz(String userTz) {
        this.userTz = userTz;
    }

    public long getUserEmailVerificationValidity() {
        return userEmailVerificationValidity;
    }

    public void setUserEmailVerificationValidity(long userEmailVerificationValidity) {
        this.userEmailVerificationValidity = userEmailVerificationValidity;
    }

    public int getUserExemptionId() {
        return userExemptionId;
    }

    public void setUserExemptionId(int userExemptionId) {
        this.userExemptionId = userExemptionId;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public int getCompany() {
        return company;
    }

    public void setCompany(int company) {
        this.company = company;
    }

    public int getUserIsDriver() {
        return userIsDriver;
    }

    public void setUserIsDriver(int userIsDriver) {
        this.userIsDriver = userIsDriver;
    }

    public int getUserIsLoggedIn() {
        return userIsLoggedIn;
    }

    public void setUserIsLoggedIn(int userIsLoggedIn) {
        this.userIsLoggedIn = userIsLoggedIn;
    }

    @Override
    public String toString() {
        return "dbUserRequest{" + "userFirstName=" + userFirstName + ", userLastName=" + userLastName + ", userNumber=" + userNumber + ", userVerifiedEmail=" + userVerifiedEmail + ", userEmail=" + userEmail + ", userEmailIsVerified=" + userEmailIsVerified + ", userPassword=" + userPassword + ", userAccessToken=" + userAccessToken + ", userTerminalId=" + userTerminalId + ", userDriverLoginId=" + userDriverLoginId + ", userHoSRule=" + userHoSRule + ", userTz=" + userTz + ", role=" + role + ", company=" + company + ", userExemptionId=" + userExemptionId + ", userEmailVerificationValidity=" + userEmailVerificationValidity + ", userIsDriver=" + userIsDriver + ", userIsLoggedIn=" + userIsLoggedIn + '}';
    }

   

   
}
