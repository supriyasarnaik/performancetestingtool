package usercreation;




/**
 *
 * @author plunawat
 */
public class LoginUserDetailsResponse {

	private int userId;

	private String userFirstName;

	private String userLastName;

	private String userNumber;

	private String userVerifiedEmail;

	private String userEmail;

	private short userEmailIsVerified;

	private String userPassword;

	private String userAccessToken;

	private int userTerminalId;

	private int userTripId;

	private short userIsDriver;

	private short userIsLoggedIn;

	private long userEmailVerificationValidity;

	private String userDriverLoginId;

	private String userTractorNumber;
	
	private String userTractorFuelType;

	private String userTrailerNumber;

	private String userHoSRule;

	private int userDeviceId;

	private long userDevicePairedDate;

	private String userMsgJID;

	private int userExemptionId;

	private String userAppVersion;

	private Role role;
	
	private String oa;
	
	private String dot;

	private Company company;
        
        private String userShippingDocNo;
        
        private String userTz;

	private short status;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserFirstName() {
		return userFirstName;
	}

	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	public String getUserLastName() {
		return userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	public String getUserNumber() {
		return userNumber;
	}

	public void setUserNumber(String userNumber) {
		this.userNumber = userNumber;
	}

	public String getUserVerifiedEmail() {
		return userVerifiedEmail;
	}

	public void setUserVerifiedEmail(String userVerifiedEmail) {
		this.userVerifiedEmail = userVerifiedEmail;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public short getUserEmailIsVerified() {
		return userEmailIsVerified;
	}

	public void setUserEmailIsVerified(short userEmailIsVerified) {
		this.userEmailIsVerified = userEmailIsVerified;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserAccessToken() {
		return userAccessToken;
	}

	public void setUserAccessToken(String userAccessToken) {
		this.userAccessToken = userAccessToken;
	}

	public short getUserIsDriver() {
		return userIsDriver;
	}

	public void setUserIsDriver(short userIsDriver) {
		this.userIsDriver = userIsDriver;
	}

	public short getUserIsLoggedIn() {
		return userIsLoggedIn;
	}

	public int getUserTerminalId() {
		return userTerminalId;
	}

	public void setUserTerminalId(int userTerminalId) {
		this.userTerminalId = userTerminalId;
	}

	public int getUserTripId() {
		return userTripId;
	}

	public void setUserTripId(int userTripId) {
		this.userTripId = userTripId;
	}

	public void setUserIsLoggedIn(short userIsLoggedIn) {
		this.userIsLoggedIn = userIsLoggedIn;
	}

	public long getUserEmailVerificationValidity() {
		return userEmailVerificationValidity;
	}

	public void setUserEmailVerificationValidity(long userEmailVerificationValidity) {
		this.userEmailVerificationValidity = userEmailVerificationValidity;
	}

	public String getUserDriverLoginId() {
		return userDriverLoginId;
	}

	public void setUserDriverLoginId(String userDriverLoginId) {
		this.userDriverLoginId = userDriverLoginId;
	}

	public String getUserTractorNumber() {
		return userTractorNumber;
	}

	public void setUserTractorNumber(String userTractorNumber) {
		this.userTractorNumber = userTractorNumber;
	}

	public String getUserTrailerNumber() {
		return userTrailerNumber;
	}

	public void setUserTrailerNumber(String userTrailerNumber) {
		this.userTrailerNumber = userTrailerNumber;
	}

	public String getUserHoSRule() {
		return userHoSRule;
	}

	public void setUserHoSRule(String userHoSRule) {
		this.userHoSRule = userHoSRule;
	}

	public int getUserDeviceId() {
		return userDeviceId;
	}

	public void setUserDeviceId(int userDeviceId) {
		this.userDeviceId = userDeviceId;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public long getUserDevicePairedDate() {
		return userDevicePairedDate;
	}

	public void setUserDevicePairedDate(long userDevicePairedDate) {
		this.userDevicePairedDate = userDevicePairedDate;
	}

	public String getUserMsgJID() {
		return userMsgJID;
	}

	public void setUserMsgJID(String userMsgJID) {
		this.userMsgJID = userMsgJID;
	}

	public int getUserExemptionId() {
		return userExemptionId;
	}

	public void setUserExemptionId(int userExemptionId) {
		this.userExemptionId = userExemptionId;
	}

	public String getUserAppVersion() {
		return userAppVersion;
	}

	public void setUserAppVersion(String userAppVersion) {
		this.userAppVersion = userAppVersion;
	}

	public String getUserTractorFuelType() {
		return userTractorFuelType;
	}

	public void setUserTractorFuelType(String userTractorFuelType) {
		this.userTractorFuelType = userTractorFuelType;
	}

    public String getUserShippingDocNo() {
        return userShippingDocNo;
    }

    public void setUserShippingDocNo(String userShippingDocNo) {
        this.userShippingDocNo = userShippingDocNo;
    }

    public String getUserTz() {
        return userTz;
    }

    public void setUserTz(String userTz) {
        this.userTz = userTz;
    }
    
	public String getOa() {
		return oa;
	}

	public void setOa(String oa) {
		this.oa = oa;
	}

	public String getDot() {
		return dot;
	}

	public void setDot(String dot) {
		this.dot = dot;
	}

	public LoginUserDetailsResponse createLoginUserDetailResponse(User user,String fuelType , String dlStateName) {
		LoginUserDetailsResponse userDetailResponse = new LoginUserDetailsResponse();
		userDetailResponse.setUserId(user.getUserId());
		userDetailResponse.setUserFirstName(user.getUserFirstName());
		userDetailResponse.setUserLastName(user.getUserLastName());
		userDetailResponse.setUserNumber(user.getUserNumber());
		userDetailResponse.setUserVerifiedEmail(user.getUserVerifiedEmail());
		userDetailResponse.setUserEmail(user.getUserEmail());
		userDetailResponse.setUserEmailIsVerified(user.getUserEmailIsVerified());
		userDetailResponse.setUserPassword(user.getUserPassword());
		userDetailResponse.setUserAccessToken(user.getUserAccessToken());
		userDetailResponse.setUserIsDriver(user.getUserIsDriver());
		userDetailResponse.setUserTerminalId(user.getUserTerminalId());
		userDetailResponse.setUserTripId(user.getUserTripId());
		userDetailResponse.setUserIsLoggedIn(user.getUserIsLoggedIn());
		userDetailResponse.setUserEmailVerificationValidity(user.getUserEmailVerificationValidity());
		userDetailResponse.setUserDriverLoginId(user.getUserDriverLoginId());
		userDetailResponse.setUserTractorNumber(user.getUserTractorNumber());
		userDetailResponse.setUserTrailerNumber(user.getUserTrailerNumber());
		userDetailResponse.setUserHoSRule(user.getUserHoSRule());
		userDetailResponse.setUserDeviceId(user.getUserDeviceId());
		userDetailResponse.setRole(user.getRole());
		userDetailResponse.setCompany(user.getCompany());
		userDetailResponse.setStatus(user.getStatus());
		userDetailResponse.setUserDevicePairedDate(user.getUserDevicePairedDate());
		userDetailResponse.setUserMsgJID(user.getUserMsgJID());
		userDetailResponse.setUserExemptionId(user.getUserExemptionId());
		userDetailResponse.setUserAppVersion(user.getUserAppVersion());
		userDetailResponse.setUserTractorFuelType(fuelType);
		userDetailResponse.setOa(user.getUserOANumber());
		userDetailResponse.setDot(user.getUserDOTNumber());
                userDetailResponse.setUserShippingDocNo(user.getUserShippingDocumentNumber());
                userDetailResponse.setUserTz(user.getUserTz());
//		IdDetails dlDetail = new IdDetails(user.getDlName(), user.getDlNum(), dlStateName, user.getDlVerified());
//		userDetailResponse.setIdDetails(dlDetail);

		return userDetailResponse;
	}


}
