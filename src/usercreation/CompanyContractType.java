/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usercreation;

import java.io.Serializable;


public class CompanyContractType implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private int id;

    private String contractName;

    private boolean status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Contract{" + "id=" + id + ", contractName=" + contractName + ", status=" + status + '}';
    }     
}
