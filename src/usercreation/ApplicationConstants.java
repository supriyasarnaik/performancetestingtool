package com.dc.ws.constants;

public interface ApplicationConstants {
    
    public static enum GpsFixType {
        NOFIX(-1),
        HASFIX(3),
        STARTGUARD(0),
        ENDGUARD(99);

        private int fixValue;

        GpsFixType(int i) {
            fixValue = i;
        }

        public int getFixValue() {
            return fixValue;
        }
    }
    
    public static enum DriverStateEnum {
        OFF_DUTY("OFF DUTY"), SLEEPER_BERTH("SLEEPER BERTH"), DRIVING("DRIVING"), ON_DUTY("ON DUTY");

        private String displayName;

        DriverStateEnum(String displayName) {
            this.displayName = displayName;
        }
    }
    
    public interface FILE_HEADER_PARAMETER {

        public static final String DEVICE_SERIAL_NUM = "dongleSerialNumber";
        public static final String TRACTOR_ID = "tractorId";
        public static final String TRAILER_ID = "trailerId";

    }

    public interface REQUEST_PARAMETERS {

        public static final String ACCESS_TOKEN = "accessToken";
        public static final String USER_ACCESS_TOKEN = "userAccessToken";
        public static final String ID = "id";
        public static final String COMPANY_DETAILS = "companyDetails";
        public static final String DEVICE_DETAILS = "deviceDetails";

        //Request parameter for HOS
        public static final String HOS_LOG = "hosLog";
        public static final String CREATE_DATE = "createdDate";
        public static final String CURRENT_STATUS = "currentStatus";
        public static final String HOS_SERVER_LOG_ID = "serverHosLogId";
        public static final String CERTIFIED_BY = "certifiedBy";

        //Request parameter for HOS event
        public static final String STATUS = "status";

        //Request parameter for HOS Violation
        public static final String VIOLATION = "violation";

        //Request parameter for Forgot Password API
        public static final String EMAIL_ID = "emailId";

    }

    public interface RESPONSE_PARAMETERS {

        public static final String RESPONSE = "response";

        public static final String COMPANY_LIST = "companyList";
        public static final String COMPANY_DETAILS = "companyDetails";

        public static final String DEVICE_LIST = "deviceList";
        public static final String DEVICE_DETAILS = "deviceDetails";

        public static final String DVIR_LIST = "dvirs";
        public static final String DVIR_SERVER_ID = "serverDvirId";

        public static final String DVIR_TRUCK_LIST = "truckDvirs";

        public static final String TRACTOR_LIST = "tractorList";
        public static final String TRACTOR_DETAILS = "tractorDetails";

        public static final String TRAILER_LIST = "trailerList";
        public static final String TRAILER_DETAILS = "trailerDetails";

        public static final String HOS_LOG = "hosLog";
        public static final String HOS_SERVER_LOG_ID = "serverHosLogId";

        public static final String TERMINAL_LIST = "terminalList";
        public static final String TERMINAL_DETAILS = "terminalDetails";

        public static final String COMPANY_CODE = "companyCode";

        public static final String USER_DETAILS = "userDetails";
        public static final String USER_LIST = "userList";
        public static final String SUBSCRIPTION_DETAILS = "subscriptionDetails";
        public static final String FIRMWARE_DETAILS = "firmwareDetails";
    }

    public interface BOOLEAN {

        public static final boolean FALSE = false;
        public static final boolean TRUE = true;
    }
    
    public interface COMPANY_CONTRACT_TYPE {

        public static final String RETAIL = "retail";
        public static final String DIRECT = "direct";

    }

    public interface DATE_FORMATS {

        // STANDARD FORMAT
        public static final String MYSQL_TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";
        public static final String MYSQL_DATE_FORMAT = "yyyy-dd-MM";
        public static final String MYSQL_TIME_FORMAT = "HH:mm";
        public static final String ISO_8601_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
        public static final String XML_CONVERTER_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";

        // OUR PRODUCT FORMAT
        public static final String TIMESTAMP_24HRS_FORMAT = "MM/dd/yyyy HH:mm:ss";
        public static final String TIMESTAMP_12HRS_FORMAT = "MM/dd/yyyy hh:mm:ss a";
        public static final String DATE_FORMAT = "MM/dd/yyyy";
        public static final String DATE_WITHOUT_YEAR_FORMAT = "MM/dd";
        public static final String TIME_24HRS_WITH_SEC_FORMAT = "HH:mm:ss";
        public static final String TIME_24HRS_FORMAT = "HH:mm";
        public static final String TIME_12HRS_FORMAT = "hh:mm a";
        public static final String TIME_12HRS_WITH_SEC_FORMAT = "hh:mm:ss a";
        public static final String WEEK_DAY_TIME_YR_FORMAT = "EEE MMM dd HH:mm:ss yyyy";
        public static final String DAY_MONTH_YR_TIME_FORMAT = "dd-MM-yyyy HH:mm:ss";

    }

    // TIMEZONE difference from GMT
    public interface TIMEZONE_DIFF_GMT {

        public static final String PST = "GMT-7:00";
        public static final String MST = "GMT-6:00";
        public static final String CST = "GMT-5:00";
        public static final String EST = "GMT-4:00";
        public static final String AST = "GMT-3:00";
    }

    // TIMEZONES
    public interface TIMEZONE {

        public static final String UTC = "UTC";
        public static final String PST = "-08:00";
        public static final String MST = "-07:00";
        public static final String CST = "-06:00";
        public static final String EST = "-05:00";
        public static final String AST = "-04:00";
    }
    
    // DST TIMEZONE
    public interface DST_TIMEZONE {

        public static final String PST = "PST8PDT";
        public static final String MST = "America/Denver";
        public static final String CST = "CST6CDT";
        public static final String EST = "EST5EDT";
        public static final String AST = "America/Halifax";

    }
    //TIMESTAMP CONSTANTS
    public interface TIMESTAMP_CONSTANTS {

        public static final long TIMESTAMP_FOR_NINETY_DAYS = 7776000000L;
        public static final long TIMESTAMP_FOR_SEVEN_DAYS = 604800000L;
        public static final long TIMESTAMP_FOR_THIRTY_MIN = 30 * 60 * 1000L;
        public static final long TIMESTAMP_FOR_FIFTEEN_MIN = 15 * 60 * 1000L;
        public static final long TIMESTAMP_FOR_EIGHT_DAYS = 691200000L;
        public static final long TIMESTAMP_FOR_FOURTEEN_DAYS = 14 * 24 * 60 * 60 * 1000L;
        public static final int SECONDS_FOR_24_HOURS = 86400;
//        public static final int SECONDS_FOR_24_HOURS = 86400;
        public static final int TIMESTAMP_FOR_ONE_DAY = 86400;
        public static final long TIMESTAMP_FOR_ONE_DAY_MILLIS = 86400000L;
        public static final long SEC_TO_MILLI_CONVERSION_VALUE = 1000;
        public static final long UTC_TIMESTAMP_30JUNE = 1467244800000L;

        public static final long TIMESTAMP_FOR_ONE_HOUR = 3600000;
        public static final long TIMESTAMP_FOR_TEN_HOUR = 36000000;
        
        public static final long TIMESTAMP_FOR_ONE_MIN = 60000;

        public static final int SECONDS_FOR_23_HOURS = 82800;
        public static final int SECONDS_FOR_25_HOURS = 90000;
    }

    //HOS EVENT STATUS CONSTANTS
    public interface HOS_EVENT_STATUS {

        public static final String ON_DUTY = "ON DUTY";
        public static final String OFF_DUTY = "OFF DUTY";
        public static final String SLEEPER_BERTH = "SLEEPER BERTH";
        public static final String DRIVING = "DRIVING";

    }

    // TIMEZONE_DB_CONSTANT 
    public interface TIMEZONE_DB_CONSTANT {

        public static final String PST = "PST";
        public static final String MST = "MST";
        public static final String CST = "CST";
        public static final String EST = "EST";
        public static final String AST = "AST";
        public static final String UTC = "UTC";
    }

    public interface USER_ROLE_INT {

        public static final int ROLE_SUPER_USER = 1;
        public static final int ROLE_OWNER_OPERATOR = 2;
        public static final int ROLE_DRIVER = 3;
        public static final int ROLE_UNASSIGNED_DRIVER = 11;
        public static final int ROLE_FLEET_MANAGER =7;
    }

    public interface USER_ROLE_STRING {

        public static final String ROLE_OWNER_OPERATOR = "ROLE_OWNER";
        public static final String ROLE_DRIVER = "ROLE_DRIVER";

    }

    public interface DEFAULT_VALUES {

        public static final String DEFAULT_TIMEZONE_CST = "GMT-6:00";
        public static final String DEFAULT_LANGUAGE = "EN";
        public static final String DEFAULT_HOS_RULE = "US 8 days/70 hours (Interstate)";
        public static final String DEFAULT_LOCALE = "en_US";
        public static final String DEFAULT_COMPANY_CODE_PREFIX = "RM-0";

    }

    public interface MANGO_COLLECTIONS {

        public static final String HOS_RULE_7DAYS = "US 7 days/60 hours (Interstate)";
        public static final String HOS_RULE_8DAYS = "US 8 days/70 hours (Interstate)";
        public static final String HOS_RULE_CANADA_CYCLE_1 = "Canada cycle-1";
        public static final String HOS_RULE_CANADA_CYCLE_2 = "Canada cycle-2";
        public static final String HOS_RULE_CALIFORNIA = "California 8 days/80 hours (Intrastate)";
        public static final String HOS_RULE_TEXAS = "Texas 7 days/70 hours (Intrastate)";

    }

    //  RESPONSE & ERROR codes
    public interface RESPONSE {

        public interface RESPONSE_TYPE {

            public static final String SUCCESS = "Success";
            public static final String EXCEPTION = "Exception";
            public static final String ERROR = "Error";
            public static final String PARTIAL_SUCCESS = "Partial Success";

        }

        public interface RESPONSE_CODES {

            public static final int HTTP_SUCCESS = 200;
            public static final int HTTP_SERVER_ERROR = 500;
            public static final int SUCCESS = 20000;
            public static final int VALID_DATA_RECEIVED = 20001;
            public static final int FAILURE = 3110;

            //Common error codes
            public static final int NOT_AUTHORISED = 33114;
            public static final int ALREADY_LOGGED_OUT = 33118;
            public static final int MALFORMED_REQUEST = 33111;
            public static final int MALFORMED_DATA = 33112;
            public static final int ZIP_ERROR = 33115;
            public static final int API_DEPRECATED = 53000;
            public static final int VALIDATAION_FAILED = 33116;
            public static final int INVALID_STATE = 33117;

            // DB/CONNECTION RELATED ERROR
            public static final int DATABASE_ERROR = 33101;
            public static final int PARAM_TYPE_ERROR = 33102;
            public static final int PARAM_ERROR = 33103;
            public static final int FOREIGN_KEY_ERROR = 33104;
            public static final int CONSTRAINT_ERROR = 33105;
            public static final int PARAM_MISMATCH_ERROR = 33106;
            public static final int NULL_OBJECT_ERROR = 33107;
            public static final int JSON_PROCESSING_ERROR = 33108;
            public static final int INPUT_OUTPUT_ERROR = 33109;
            public static final int NOT_UNIQUE_RESULT_ERROR = 33110;
            public static final int UNKNOWN_ERROR = 33113;

            // User ERROR codes 
            public static final int USER_NOT_LOGGED_IN_OR_AUTHORISED = 33200;
            public static final int TRACTOR_NUMBER_REQUIRED = 33201;
            public static final int USER_FIELDS_REQUIRED = 33203;
            public static final int DRIVER_ID_REQUIRED = 33204;
            public static final int EMAIL_ID_REQUIRED = 33205;
            public static final int PASSWORD_REQUIRED = 33206;
            public static final int USER_REGISTRATION_FAILED = 33207;
            public static final int USER_ALREADY_REGISTERED = 33208;
            public static final int INVALID_USER_DATA = 33209;
            public static final int USER_CREATE_FAILED = 33210;
            public static final int CANNOT_UPDATE_PASSWORD = 33211;
            public static final int CANNOT_UPDATE_ACCESSTOKEN = 33212;
            public static final int CANNOT_UPDATE_DRIVER_ID = 33213;
            public static final int USER_UPDATE_FAILED = 33214;
            public static final int AUTHENTICATION_FAILED = 33215;
            public static final int ALREADY_LOGIN_FROM_APP = 33216;
            public static final int USER_LIST_NOT_FOUND = 33217;
            public static final int USER_DELETE_FAILED = 33218;
            public static final int USER_NOT_FOUND = 33219;
            public static final int VEHICLE_UPDATE_FAILED = 33220;
            public static final int SEND_EMAIL_FAILED = 33222;
            public static final int PASSWORD_UPDATE_FAILED = 33223;
            public static final int EMAIL_NOT_REGISTERED = 33224;
            public static final int INVALID_USER_CREDENTIALS = 33225;
            public static final int INVALID_USER = 33226;
            public static final int INVALID_LOGIN_CREDENTIALS = 33227;
            public static final int SUBSCRIPTION_EXPIRED = 33228;
            public static final int CSV_FILE_NAME_REQUIRED = 33229;
            public static final int FILE_NOT_FOUND = 33230;
            public static final int EMAIL_ALREADY_VERIFIED = 33231;
            public static final int SUBSCRIPTION_UPGRADE_FAILED = 33232;

            public static final int FAILED_TO_VERIFY_EMAIL = 33233;
            public static final int EMAIL_VERIFICATION_LINK_EXPIRED = 33234;

            public static final int FILE_ALREADY_UPLOADED = 33235;
            public static final int USER_HOS_RULE_UPDATE_ERROR = 33236;
            
            public static final int DUPLICATE_DL_NUMBER = 33237;
            
            public static final int FAILED_TO_READ_CSV_FILE = 33238;
            
            public static final int NO_LIST_FOUND=33239;
            public static final int CODRIVER_UPDATE_ERROR= 33240;
            public static final int CODRIVER_NOT_FOUND= 33241;
            public static final int CODRIVER_ALREADY_ENABLE= 33242;
            public static final int CODRIVER_TRACTOR_DOES_NOT_MATCH= 33243;

            //USER success codes
            public static final int PARTIAL_COMPANY_UPDATE_FAILED = 23200;
            public static final int PARTIAL_DEVICE_UPDATE_FAILED = 23201;
            public static final int USER_REGISTERED = 23202;
            public static final int USER_CREATED = 23203;
            public static final int USER_UPDATED = 23204;
            public static final int USER_LOGGED_IN = 23205;
            public static final int USER_LOGGED_OUT = 23206;
            public static final int USER_DELETED = 23207;
            public static final int VEHICLE_COMPANY_UPDATED = 23208;
            public static final int ONLY_COMPANY_UPDATED = 23209;
            public static final int ONLY_VEHICLE_UPDATED = 23210;
            public static final int VEHICLE_UPDATED = 23211;
            public static final int PASSWORD_SENT = 23214;
            public static final int NOTIFICATION_FOUND = 23215;
            public static final int NOTIFICATION_NOT_FOUND = 23216;
            public static final int CSV_FILE_READ_SUCCESSFUL = 23217;
            public static final int EMAIL_VERIFIED_SUCCESSFULLY = 23218;
            public static final int EMAIL_SENT_SUCCESS = 23219;
            public static final int COMPANY_INFO_SENT = 23220;
            public static final int USER_HOS_RULE_UPDATE_SUCCESS = 23221;
            public static final int CODRIVER_UPDATE_SUCCESSFULLY=23222;

            // Device ERROR Codes
            public static final int DEVICE_FIELDS_REQUIRED = 33300;
            public static final int DEVICE_SERIAL_NUMBER_REQUIRED = 33301;
            public static final int DEVICE_BLUETOOTH_MAC_REQUIRED = 33302;
            public static final int DEVICE_WIFI_MAC_REQUIRED = 33303;
            public static final int DEVICE_IMEI_REQUIRED = 33304;
            public static final int DEVICE_TYPE_REQUIRED = 33305;
            public static final int SERIAL_NUMBER_CAN_NOT_UPDATED = 33306;
            public static final int IMEI_NUMBER_CAN_NOT_UPDATED = 33307;
            public static final int BLUETOOTH_MAC_CAN_NOT_UPDATED = 33308;
            public static final int FAILED_TO_UPDATE_DEVICE = 33309;
            public static final int DEVICE_LIST_NOT_FOUND = 33310;
            public static final int DEVICE_NOT_FOUND = 33311;
            public static final int DEVICE_DELETE_OPERATION_FAILED = 33312;
            public static final int PROBLEM_IN_DEVICE_REGISTRATION = 33313;
            public static final int ALREADY_REGISTERED_WITH_ANOTHER_COMPANY = 33314;
            public static final int DEVICE_NOT_SUBSCRIBED = 33315;
            public static final int NEW_DEVICE = 33316;
            public static final int FAILED_TO_REGISTER_DEVICE = 33317;
            public static final int VERIFY_EMAIL_TO_REGISTER_DEVICE = 33318;
            public static final int SUBSCRIBE_DEVICE_FROM_PORTAL = 33319;
            public static final int DC200_DEVICE_FROM_PORTAL = 33320;
            public static final int DIRECT_CHANNEL_COMPANY_DEVICE_REGISTRATION = 33321;

            //Device Success codes
            public static final int DEVICE_UPDATED = 23300;
            public static final int DEVICE_DELETED = 23301;
            public static final int DEVICE_REGISTERED = 23302;
            public static final int AUTHENTICATED_TO_USE_DEVICE = 23303;

            // DVIR Error codes
            public static final int INVALID_VEHICLE_DETAILS = 33401;
            public static final int INVALID_DVIR_DATA = 33402;
            public static final int DVIR_CERTIFICATION_FAILED = 33403;
            public static final int DVIR_IS_ALREADY_CERTIFIED = 33404;
            public static final int INVALID_TRACTOR_NUMBER = 33405;
            public static final int TRAILER_NUMBER_NOT_FOUND = 33406;
            public static final int CERTIFY_DVIR_MECH_SIGN_MANDATORY = 33407;
            public static final int FAILED_TO_CREATE_DVIR = 33408;
            public static final int INVALID_TRAILER_LENGTH = 33409;
            public static final int TRACTOR_NUMBER_MANDATORY = 33410;
            public static final int INVALID_TRACTOR_LENGTH = 334011;
            //Offline Support
            public static final int DVIR_SYNC_FAILED = 334012;
            public static final int INVALID_REQUEST=334013;
            // DVIR Partial Success codes
            //Offline Support
            public static final int DVIR_SYNC_CREATE_FAILED = 43401;
            public static final int DVIR_SYNC_CERTIFY_FAILED = 43402;
            public static final int DVIR_SYNC_CREATE_AND_CERTIFY_FAILED = 43403;

            // DVIR Success codes
            public static final int DVIR_CREATED = 23400;
            public static final int DVIR_CERTIFIED = 23401;
            public static final int VALID_DVIR_DATA = 23402;
            public static final int DVIR_LIST_NOT_FOUND = 23403;
            //Offline Mode
            public static final int DVIR_SYNC_SUCCESS = 23404;

            // Event ERROR codes
            public static final int FAILED_TO_CREATE_EVENT = 34000;
            public static final int INVALID_LATITUDE_LONGITUDE = 34001;
            public static final int INVALID_DEVICE = 34002;

            // Event Success codes
            public static final int EVENT_DATA_LOGGED = 24000;
            public static final int VALID_EVENT_DATA = 24001;
            public static final int NO_EVENT_DATA_FOUND = 24002;
            public static final int EVENT_DATA_FOUND = 24003;

            // ELD-Event ERROR codes
            public static final int FAILED_TO_CREATE_ELD_EVENT = 36000;

            // ELD-Event Success codes
            public static final int ELD_EVENT_DATA_LOGGED = 26000;
            public static final int NO_ELD_EVENT_DATA_FOUND = 26002;
            
            
            // HOS ERROR codes
            public static final int HOS_ALREADY_EXISTS = 33900;
            public static final int FAILED_TO_CREATE_HOS = 33901;
            public static final int CREATED_DATE_REQUIRED = 33902;

            public static final int FAILED_TO_UPDATE_HOS = 33904;
            public static final int CANNOT_UPDATE_CERTIFIED_HOS = 33905;
            public static final int HOS_DOES_NOT_EXIST = 33906;
            public static final int FAILED_TO_CERTIFY_HOS = 33907;
            public static final int MINIMUM_ONE_HOS_ID_REQUIRED = 33908;
            public static final int HOS_ID_CERTIFY_BY_FIELDS_REQUIRED = 33909;
            public static final int HOS_ID_NOT_AVAILABLE = 33910;
            public static final int FAILED_TO_LOG_HOS_VIOLATION = 33911;
            public static final int FAILED_TO_LOG_HOS_STATUS = 33912;
            public static final int INVALID_HOS_STATUS = 33913;
            public static final int INVALID_HOS_SERVER_LOG_ID = 33914;
            public static final int HOS_VIOLATION_CREATED_DATE_MANDATORY = 33915;
            public static final int HOS_VIOLATION_ID_MANDATORY = 33916;
            public static final int INVALID_HOS_CURRENT_STATUS = 33917;
            public static final int CURRENT_HOS_STATUS_MANDATORY = 33918;
            public static final int CREATE_HOS_STATUS_END_TIMESTAMP_MANDATORY = 33919;
            public static final int CREATE_HOS_STATUS_INVALID_DURATION = 33920;
            public static final int CREATE_HOS_STATUS_INVALID_COMMENT_LENGTH = 33921;
            public static final int CREATE_HOS_STATUS_INVALID_LOCATION_NAME_LENGTH = 33922;
            public static final int UPDATE_HOS_FAILED_INVALID_STATUS_DETAILS = 33923;
            public static final int UPDATE_HOS_FAILED_INVALID_VIOLATION_DETAILS = 33924;
            public static final int INVALID_HOS_CERTIFY_BY_FIELD = 33925;
            public static final int NEGATIVE_HOS_STATUS_DURATION = 33926;
            public static final int INACTIVE_HOS = 33927;

            // HOS Success codes
            public static final int HOS_LOGGED = 23900;
            public static final int HOS_UPDATED = 23901;
            public static final int HOS_CERTIFIED = 23902;
            public static final int HOS_STATUS_LOGGED = 23903;
            public static final int HOS_VIOLATION_LOGGED = 23904;
            public static final int NO_HOS_DATA_FOUND = 23905;
            public static final int VALID_HOS_VIOLATION_OBJECT = 23905;
            public static final int VALID_HOS_STATUS_OBJECT = 23906;
            public static final int PARTIAL_HOS_UPDATES = 23907;

            // Company ERROR codes
            public static final int COMPANY_FIELDS_REQUIRED = 33500;
            public static final int COMPANY_EMAIL_REQUIRED = 33501;
            public static final int COMPANY_LIST_NOT_FOUND = 33503;
            public static final int COMPANY_NOT_FOUND = 33504;
            public static final int COMPANY_CREATE_FAILED = 33505;
            public static final int INVALID_COMPANY_DETAILS = 33506;
            public static final int COMPANY_DELETE_FAILED = 33507;
            public static final int COMPANY_UPDATE_FAILED = 33508;
            public static final int COMPANY_WITH_DOT_ALREADY_EXISTS = 33509;
            public static final int COMPANY_EMAIL_ALREADY_VERIFIED = 33510;
            public static final int COMPANY_INVALID_ZIP=33511;
            
            // Company Success codes
            public static final int COMPANY_DELETED = 23500;
            public static final int COMPANY_CREATED = 23501;
            public static final int COMPANY_UPDATED = 23502;
            public static final int COMPANY_WITH_DOT_DOSENOT_EXISTS = 23509;

            // Terminal ERROR codes
            public static final int TERMINAL_LIST_NOT_FOUND = 33800;
            public static final int TERMINAL_NOT_FOUND = 33801;
            public static final int TERMINAL_CREATE_FAILED = 33802;
            public static final int TERMINAL_UPDATE_FAILED = 33803;
            public static final int TERMINAL_DELETE_FAILED = 33804;
            public static final int TERMINAL_FIELDS_REQUIRED = 33805;
            public static final int TERMINAL_NAME_REQUIRED = 33806;
            public static final int TERMINAL_DOT_REQUIRED = 33807;
            public static final int TERMINAL_ADDRESS_REQUIRED = 33808;
            public static final int TERMINAL_CONTACT_NUMBER_REQUIRED = 33809;
            public static final int TERMINAL_TIMEZONE_REQUIRED = 33810;

            // Terminal SUCCESS codes
            public static final int TERMINAL_CREATED = 23800;
            public static final int TERMINAL_UPDATED = 23801;
            public static final int TERMINAL_DELETED = 23802;

            //Tractor Error codes
            public static final int TRACTOR_LIST_NOT_FOUND = 33600;
            public static final int TRACTOR_ID_NOT_FOUND = 33601;
            public static final int FAILED_TO_CREATE_TRACTOR = 33602;
            public static final int FAILED_TO_UPDATE_TRACTOR = 33603;
            public static final int FAILED_TO_DELETE_TRACTOR = 33604;
            public static final int TRACTOR_FIELDS_REQUIRED = 33605;
            public static final int TRACTOR_NUMBER_FIELD_REQUIRED = 33606;

            //Tractor Success codes
            public static final int TRACTOR_CREATED = 23600;
            public static final int TRACTOR_UPDATED = 23601;
            public static final int TRACTOR_DELETED = 23602;

            //Trailer Error messages
            public static final int TRAILER_FIELDS_REQUIRED = 33700;
            public static final int TRAILER_NUMBER_FIELD_REQUIRED = 33701;
            public static final int TRAILER_LIST_NOT_FOUND = 33702;
            public static final int TRAILER_ID_NOT_FOUND = 33703;
            public static final int FAILED_TO_ADD_TRAILER = 33704;
            public static final int FAILED_TO_UPDATE_TRAILER = 33705;
            public static final int FAILED_TO_DELETE_TRAILER = 33706;

            //Trailer Success messages
            public static final int TRAILER_CREATED = 23700;
            public static final int TRAILER_UPDATED = 23701;
            public static final int TRAILER_DELETED = 23702;

            //BOL Success messages
            public static final int NO_BoL_DATA_FOUND = 23800;

            //IFTA Success code
            public static final int IFTA_CREATED = 24000;

            //IFTA Error code
            public static final int FAILED_TO_CREATE_IFTA = 34000;
            public static final int IFTA_IMAGE_NOT_FOUND = 34001;
            public static final int INVALID_IFTA_DATA = 34002;
            public static final int IFTA_ALREADY_EXISTED = 34003;
            public static final int IFTA_DETAILS_NOT_FOUND = 34004;
            public static final int IFTA_LIST_NOT_FOUND = 34005;
           // public static final int IFTA_NO_DATA_FOUND=34006;

            //MOA error Code
            public static final int NO_MOA_FOUND=35000; 
            
            //Subscription Error messages 
            public static final int SUBSCRIPTIONS_EXPIRED = 33900;
            public static final int GRACE_SUBSCRIPTIONS_EXPIRED = 33901;

            //Messaging Succes Codes
            public static final int OPENFIRE_USER_CREATED = 25000;
            public static final int OPENFIRE_USER_PRESENT = 25001;
            public static final int MESSAGES_FOUND = 25002;

            //Messaging Error Code
            public static final int OPENFIRE_USER_NOT_CREATED = 35000;
            public static final int NO_MESSAGES_FOUND = 35002;
            
            //Timezone Success Codes
            public static final int TIMEZONE_DATA_FETCHED = 27000;
            public static final int TIMEZONE_UPDATED = 27001;
            
            //Timezone Error Codes
            public static final int FAILED_TO_FETCH_TIMEZONE_DATA = 37000;
            public static final int FAILED_TO_UPDATED_TIMEZONE = 37001;
            
            //Timezone Success Codes
            public static final int HOS_MILES_DATA_FETCHED = 27100;
            public static final int HOS_MILES_UPDATED = 27101;
            
            //HoS Miles codes
            public static final int FAILED_TO_FETCH_HOS_MILES_DATA = 37100;
            public static final int FAILED_TO_UPDATED_HOS_MILES = 37101;
            
            //Driving Status success code
            public static final int FAULT_RECOVER_DRIVING_STATUS_CREATED = 27200;
            public static final int FAULT_RECOVER_DRIVING_STATUS_FETCHED = 27201;
            
            //Driving Status error code
            public static final int FAILED_TO_CREATE_FAULT_RECOVER_DRIVING_STATUS = 37200;
            public static final int FAILED_TO_FETCH_FAULT_RECOVER_DRIVING_STATUS = 37201;
            //Manual HoS status success codes
            public static final int MANUAL_STATUS_DATA_UPDATED_SUCCESSFULLY = 27300;
            
            //Manual HoS status error codes
            public static final int FAILED_TO_UPDATE_MANUAL_STATUS_DATA = 37300;
            
        }

        public interface RESPONSE_MESSAGE {

            // DB/CONNECTION RELATED ERROR
            public static final String DATABASE_ERROR = "Database error ";
            public static final String PARAM_TYPE_ERROR = "Param type error";
            public static final String PARAM_ERROR = "Param error";
            public static final String FOREIGN_KEY_ERROR = "Foreign key error";
            public static final String CONSTRAINT_ERROR = "Constraint error";
            public static final String PARAM_MISMATCH_ERROR = "Param mismatch error";
            public static final String NULL_OBJECT_ERROR = "object having null value";
            public static final String JSON_PROCESSING_ERROR = "Json Processing Exception";
            public static final String INPUT_OUTPUT_ERROR = "Input Output error";
            public static final String NOT_UNIQUE_RESULT_ERROR = "query did not return a unique result";
            public static final String UNKNOWN_ERROR = "Unknown error";

            // Common messages
            public static final String NOT_AUTHORISED = "User are not authorized to perform this action";
            public static final String ALREADY_LOGGED_OUT = "User has logged out successfully before this request";
            public static final String SUCCESS = "Request processed successfully";
            public static final String EMAIL_ERROR = "No Email Event Found";

            public static final String MALFORMED_REQUEST = "Malformed Request";
            public static final String MALFORMED_DATA = "The Rand McNally DriverConnect web service failed possibly due to app and web service versions cannot inter-operate. Please update the app and try again";
            public static final String VALID_DATA_RECEIVED = "Valid data received";
            public static final String API_DEPRECATED = "Please update your App";

            public static final String ZIP_ERROR = "Zip file operation error";
            
            public static final String INVALID_STATE = "Invalid state details";

            // User ERROR messages 
            public static final String USER_NOT_LOGGED_IN_OR_AUTHORISED = "Malformed Request - User either not logged in or User not authorised";
            public static final String TRACTOR_NUMBER_REQUIRED = "Tractor Number Required";
            public static final String USER_FIELDS_REQUIRED = "User fields required";
            public static final String DRIVER_ID_REQUIRED = "Driver-Id field required";
            public static final String EMAIL_ID_REQUIRED = "Email field required";
            public static final String PASSWORD_REQUIRED = "Password field required";
            public static final String USER_REGISTRATION_FAILED = "User registration failed";
            public static final String USER_ALREADY_REGISTERED = "User registration failed - User is already registered";
            public static final String INVALID_USER_DATA = "Invalid User data";
            public static final String USER_CREATE_FAILED = "Malformed request - User create operation failed";
            public static final String CANNOT_UPDATE_PASSWORD = "Cannot update password";
            public static final String CANNOT_UPDATE_ACCESSTOKEN = "AccessToken cannot be updated";
            public static final String CANNOT_UPDATE_DRIVER_ID = "DriverId cannot be updated";
            public static final String USER_UPDATE_FAILED = "Failed to update User";
            public static final String AUTHENTICATION_FAILED = "Authentication failed; please check the username and password and try again";
            public static final String ALREADY_LOGIN_FROM_APP = "You are already logged in from the Rand McNally DriverConnect app on another device. If you log in the other session will be logged out. Do you wish to continue this Log in?";
            public static final String USER_LIST_NOT_FOUND = "User List not found";
            public static final String USER_DELETE_FAILED = "User Delete Operation failed";
            public static final String USER_NOT_FOUND = "User not found";
            public static final String VEHICLE_UPDATE_FAILED = "Update to vehicle information failed";
            public static final String SEND_EMAIL_FAILED = "Please contact your company administrator to reset your password or please try again";
            public static final String PASSWORD_UPDATE_FAILED = "Please contact your company administrator to reset your password or please try again";
            public static final String EMAIL_NOT_REGISTERED = "Your email is not registered with Rand Mcnally Driverconnect. Please contact your company administrator";
            public static final String INVALID_USER_CREDENTIALS = "Invalid user credentials";
            public static final String INVALID_USER = "Notification cannot be found: not a valid user";
            public static final String INVALID_LOGIN_CREDENTIALS = "Authentication failed; please check the username and password and try again";
            public static final String SUBSCRIPTION_EXPIRED = "Your subscription to the Rand McNally DriverConnect service has expired. Please renew your subscription and try again";
            public static final String CSV_FILE_NAME_REQUIRED = "CSV file name field required";
            public static final String FILE_NOT_FOUND = "File not found";
            public static final String USER_HOS_RULE_UPDATE_FAILED = "Update to user HoS Rule failed";

            public static final String EMAIL_ALREADY_VERIFIED = "Email already verified";
            public static final String SUBSCRIPTION_UPGRADE_FAILED = "Failed to upgrade subscription";

            public static final String FAILED_TO_VERIFY_EMAIL = "Failed to verify email";
            public static final String EMAIL_VERIFICATION_LINK_EXPIRED = "Email verification link has expired";

            public static final String FILE_ALREADY_UPLOADED = "File already uploaded";
            
            public static final String INVALID_DL_NUMBER_LEGTH = "License number should be greater than 2 characters";
            public static final String INVALID_DL_NAME_LEGTH = "Driver Name should be greater than 2 characters";
            
            public static final String DUPLICATE_DL_NUMBER = "Invalid driver license details (duplicate license details)";
            
            public static final String FAILED_TO_READ_CSV_FILE = "Failed to read csv file";

            // User SUCCESS messages 
            public static final String PARTIAL_COMPANY_UPDATE_FAILED = "Partial Success - Cannot update company details";
            public static final String PARTIAL_DEVICE_UPDATE_FAILED = "Partial Success - Cannot update device details";
            public static final String USER_REGISTERED = "Thank you for registering with Rand McNally DriverConnect. Your company code is:";
            public static final String USER_CREATED = "User created Successfully";
            public static final String USER_UPDATED = "User updated successfully";
            public static final String USER_LOGGED_IN = "Welcome to Rand McNally DriverConnect. Your are connected to the DriverConnect Web Service";
            public static final String USER_LOGGED_OUT = "User logged out successfully";
            public static final String USER_DELETED = "User deleted successfully";
            public static final String VEHICLE_COMPANY_UPDATED = "Vehicle and company information have been updated successfully";
            public static final String ONLY_COMPANY_UPDATED = "Company information updated successfully. Vehicle information update has failed";
            public static final String ONLY_VEHICLE_UPDATED = "Vehicle information updated successfully. Company information update has failed";
            public static final String VEHICLE_UPDATED = "Vehicle information has updated successfully";
            public static final String PASSWORD_SENT = "Your password has been reset; please check your email";
            public static final String NOTIFICATION_FOUND = "Notifications found";
            public static final String NOTIFICATION_NOT_FOUND = "No Notifications were found";
            public static final String EMAIL_VERIFIED_SUCCESSFULLY = "Email verified successfully";
            public static final String CSV_FILE_READ_SUCCESSFUL = "CSV File read operation successful";
            public static final String EMAIL_SENT_SUCCESS = "Email sent successfully";
            public static final String USER_HOS_RULE_UPDATE_SUCCESS = "User HoS Rule has updated successfully";

            public static final String COMPANY_INFO_SENT = "Company Info sent to your email successfully";

            // Device ERROR Messages
            public static final String DEVICE_FIELDS_REQUIRED = "Device fields required";
            public static final String DEVICE_SERIAL_NUMBER_REQUIRED = "Device serial number required";
            public static final String DEVICE_BLUETOOTH_MAC_REQUIRED = "Device Bluetooth MAC required";
            public static final String DEVICE_WIFI_MAC_REQUIRED = "Device WIFI MAC required";
            public static final String DEVICE_IMEI_REQUIRED = "Device IMEI number required";
            public static final String DEVICE_TYPE_REQUIRED = "Device type required";
            public static final String SERIAL_NUMBER_CAN_NOT_UPDATED = "Device serial number cannot be updated";
            public static final String IMEI_NUMBER_CAN_NOT_UPDATED = "Device IMEI number cannot be updated";
            public static final String BLUETOOTH_MAC_CAN_NOT_UPDATED = "Device bluetooth MAC cannot be updated";
            public static final String FAILED_TO_UPDATE_DEVICE = "Failed to update device";
            public static final String DEVICE_LIST_NOT_FOUND = "Device list not found";
            public static final String DEVICE_NOT_FOUND = "Invalid device details – Please call 1-877-446-4863 to activate the device";
            public static final String DEVICE_DELETE_OPERATION_FAILED = "Device delete operation failed";
            public static final String PROBLEM_IN_DEVICE_REGISTRATION = "Device registration failed – Please call 1-877-446-4863 with the device serial number to activate the device";
            public static final String ALREADY_REGISTERED_WITH_ANOTHER_COMPANY = "Device registration failed – Device is already registered with another company. Please call 1-877-446-4863 with your device serial number to activate the ELD 50";
            public static final String DEVICE_NOT_SUBSCRIBED = "To use this device with Driverconnect service please call customer care (1-877-446-4863)";
            public static final String NEW_DEVICE = "To use this device with the DriverConnect portal, please call 1-877-446-4863";
            public static final String FAILED_TO_REGISTER_DEVICE = "Device registration failed";
            public static final String VERIFY_EMAIL_TO_REGISTER_DEVICE = "Please verify your email id to register ELD50";
            public static final String SUBSCRIBE_DEVICE_FROM_PORTAL = "Please subscribe your device from DriverConnect Portal";
            public static final String DC200_DEVICE_FROM_PORTAL = "Please subscribe your device from Portal";
            public static final String DIRECT_CHANNEL_COMPANY_DEVICE_REGISTRATION = "To use this device with the DriverConnect portal, please call 1-877-446-4863";
            public static final String FAILED_TO_UPDATE_CO_DRIVER="FAILED TO UPDATE CoDriver";
            public static final String CO_DRIVER_NOT_FOUND="CoDriver Not Found";
            public static final String CO_DRIVER_ALRADY_ENABLE="Already Enable";
            public static final String NO_CO_DRIVER_LIST_FOUND="No CoDriver list found";
            public static final String CODRIVER_TRACTOR_DOES_NOT_MATCH= "CoDriver tractor does not match";
            //Device Success Messages
            public static final String DEVICE_UPDATED = "Device updated successfully";
            public static final String DEVICE_DELETED = "Device deleted successfully";
            public static final String DEVICE_REGISTERED = "Device successfully registered";
            public static final String AUTHENTICATED_TO_USE_DEVICE = "This device is successfully associated with the ELD 50";

            //DVIR ERROR messages
            public static final String INVALID_VEHICLE_DETAILS = "Invalid vehicle details";
            public static final String INVALID_DVIR_DATA = "Unable to certify; invalid DVIR data";
            public static final String DVIR_CERTIFICATION_FAILED = "DVIR certification failed";
            public static final String DVIR_IS_ALREADY_CERTIFIED = "Unable to certify; DVIR is already certified";
            public static final String INVALID_TRACTOR_NUMBER = "DVIR cannot be stored in DriverConnect; invalid tractor number";
            public static final String TRAILER_NUMBER_NOT_FOUND = "DVIR cannot be stored in DriverConnect; trailer number not found";
            public static final String CERTIFY_DVIR_MECH_SIGN_MANDATORY = "Certify DVIR - Mechanic signature is mandatory";
            public static final String FAILED_TO_CREATE_DVIR = "Failed to create DVIR";
            public static final String INVALID_TRACTOR_LENGTH = "Invalid Tractor length";
            public static final String INVALID_TRAILER_LENGTH = "Invalid Trailer length";
            public static final String TRACTOR_NUMBER_MANDATORY = "Tractor number is mandatory";
            //Offline Support
            public static final String DVIR_SYNC_FAILED = "DVIR sync - Failed to sync with server";

            // DVIR Partial Success codes
            //Offline Support
            public static final String DVIR_SYNC_CREATE_FAILED = "DVIR sync - failed to create DVIR";
            public static final String DVIR_SYNC_CERTIFY_FAILED = "DVIR sync - failed to certify DVIR";
            public static final String DVIR_SYNC_CREATE_AND_CERTIFY_FAILED = "DVIR sync - failed to create and certify DVIR";

            // DVIR Success messages
            public static final String DVIR_CREATED = "DVIR stored successfully in DriverConnect";
            public static final String DVIR_CERTIFIED = "DVIR certified successfully";
            public static final String VALID_DVIR_DATA = "DVIR data is valid";
            public static final String DVIR_LIST_NOT_FOUND = "The DVIR cannot be retrieved from DriverConnect; DVIR list not found";
            //Offline Support
            public static final String DVIR_SYNC_SUCCESS = "DVIR sync - successful";

            // Event ERROR Messages
            public static final String FAILED_TO_CREATE_EVENT = "Event data could not be stored in DriverConnect; logging of event data failed";
            public static final String INVALID_LATITUDE_LONGITUDE = "Invalid latitude and longitude";
            public static final String INVALID_DEVICE = "Invalid device";
            
            // ELD Event ERROR Messages
            public static final String FAILED_TO_CREATE_ELD_EVENT = "ELD Event data could not be stored in DriverConnect; logging of event data failed";

            // Event Success Messages
            public static final String EVENT_DATA_LOGGED = "Event data logged successfully in DriverConnect";
            public static final String VALID_EVENT_DATA = "Valid event data";
            public static final String NO_EVENT_DATA_FOUND = "No User Event data found";
            public static final String EVENT_DATA_FOUND = "User Event data found";

            public static final String ELD_EVENT_DATA_LOGGED =  "ELD Event data logged successfully in DriverConnect";
            public static final String VALID_ELD_EVENT_DATA = "Valid ELD event data";
            public static final String NO_ELD_EVENT_DATA_FOUND = "No User ELD Event data found";
            public static final String ELD_EVENT_DATA_FOUND = "User ELD Event data found";
            
            
            
            // HOS ERROR messages
            public static final String HOS_ALREADY_EXISTS = "HOS data could not be stored in DriverConnect; HOS data already exists";
            public static final String FAILED_TO_CREATE_HOS = "HOS data could not be stored in DriverConnect";
            public static final String CREATED_DATE_REQUIRED = "HOS data could not be stored in DriverConnect; date of creation is required";

            public static final String FAILED_TO_UPDATE_HOS = "HOS data cannot be updated in DriverConnect";
            public static final String CANNOT_UPDATE_CERTIFIED_HOS = "Certified HOS data cannot be updated in DriverConnect";
            public static final String HOS_DOES_NOT_EXIST = "HOS data cannot be updated in DriverConnect; HOS does not exist";
            public static final String INACTIVE_HOS ="Inactive HoS";
            public static final String FAILED_TO_CERTIFY_HOS = "HOS data cannot be certified";
            public static final String MINIMUM_ONE_HOS_ID_REQUIRED = "At least one HOS log is required to certify HOS data";
            public static final String HOS_ID_CERTIFY_BY_FIELDS_REQUIRED = "HOS data cannot be certified; 'certify by' input parameter is required";
            public static final String HOS_ID_NOT_AVAILABLE = "HOS status cannot be stored in DriverConnect; invalid HOS log information";
            public static final String FAILED_TO_LOG_HOS_VIOLATION = "HOS violation cannot be stored in DriverConnect; creation of the HOS violation failed";
            public static final String FAILED_TO_LOG_HOS_STATUS = "HOS status cannot be stored in DriverConnect; there was an error during creation of the HOS status";
            public static final String INVALID_HOS_STATUS = "Invalid HOS Status";
            public static final String INVALID_HOS_SERVER_LOG_ID = "Invalid HOS Server log Id";
            public static final String HOS_VIOLATION_CREATED_DATE_MANDATORY = "Created date required to create HOS Violation";
            public static final String HOS_VIOLATION_ID_MANDATORY = "Violation Id required to create HOS Violation";
            public static final String INVALID_HOS_CURRENT_STATUS = "Invalid Current HOS Status";
            public static final String CURRENT_HOS_STATUS_MANDATORY = "Current HOS status is mandatory for creating HOS Status";
            public static final String CREATE_HOS_STATUS_END_TIMESTAMP_MANDATORY = "Create HOS Status - end timestamp is mandatory";
            public static final String CREATE_HOS_STATUS_INVALID_DURATION = "Create HOS Status - invalid status duration";
            public static final String CREATE_HOS_STATUS_INVALID_COMMENT_LENGTH = "Create HOS Status - invalid length of comment";
            public static final String CREATE_HOS_STATUS_INVALID_LOCATION_NAME_LENGTH = "Create HOS Status - invalid length of location name";
            public static final String UPDATE_HOS_FAILED_INVALID_STATUS_DETAILS = "Update HOS failed - invalid HOS status details";
            public static final String UPDATE_HOS_FAILED_INVALID_VIOLATION_DETAILS = "Update HOS failed - invalid HOS violation details";
            public static final String INVALID_HOS_CERTIFY_BY_FIELD = "Failed to certify HOS - Certify by field is required, must have alpha numeric values and length must be between 4 -100";
            public static final String NEGATIVE_HOS_STATUS_DURATION = "Failed to log HoS - negative HoS Status duration";

            // HOS Success messages
            public static final String HOS_LOGGED = "HOS data successfully stored in DriverConnect";
            public static final String HOS_UPDATED = "HOS data successfully updated in DriverConnect";
            public static final String HOS_CERTIFIED = "HOS data certified successfully";
            public static final String HOS_STATUS_LOGGED = "HOS status stored in DriverConnect service";
            public static final String HOS_VIOLATION_LOGGED = "HOS violation successfully stored in DriverConnect";
            public static final String NO_HOS_DATA_FOUND = "No HOS data found";
            public static final String VALID_HOS_VIOLATION_OBJECT = "HOS violation details stored successfully in DriverConnect";
            public static final String VALID_HOS_STATUS_OBJECT = "Valid HOS Status object";
            public static final String PARTIAL_HOS_UPDATES = "Partial success - HOS updated successfully";

            // Company ERROR Messages
            public static final String COMPANY_FIELDS_REQUIRED = "Company fields required";
            public static final String COMPANY_EMAIL_REQUIRED = "Company contact email field required";
            public static final String COMPANY_LIST_NOT_FOUND = "Malformed request - Company list not found";
            public static final String COMPANY_NOT_FOUND = "Malformed request - Company Not found";
            public static final String COMPANY_CREATE_FAILED = "Malformed request - new company create operation failed";
            public static final String INVALID_COMPANY_DETAILS = "Invalid company details";
            public static final String COMPANY_DELETE_FAILED = "Malformed request - Company delete operation failed";
            public static final String COMPANY_UPDATE_FAILED = "Update to company information failed";
            public static final String COMPANY_WITH_DOT_ALREADY_EXISTS = "Company with DOT already registered";
            public static final String COMPANY_EMAIL_ALREADY_VERIFIED = "Email already verified";

            // Company Success Messages
            public static final String COMPANY_DELETED = "Company deleted successfully";
            public static final String COMPANY_CREATED = "Company added Successfully";
            public static final String COMPANY_UPDATED = "Company information has updated successfully";
            public static final String COMPANY_WITH_DOT_DOSENOT_EXISTS = "New Company DOT found";

            // Terminal ERROR Messages
            public static final String TERMINAL_LIST_NOT_FOUND = "Malformed request - Terminal list not found";
            public static final String TERMINAL_NOT_FOUND = "Malformed request - Terminal Not found";
            public static final String TERMINAL_CREATE_FAILED = "Malformed request - new Terminal create operation failed";
            public static final String TERMINAL_UPDATE_FAILED = "Malformed request - Failed to update Terminal";
            public static final String TERMINAL_DELETE_FAILED = "Malformed request - Terminal delete operation failed";
            public static final String TERMINAL_FIELDS_REQUIRED = "Terminal fields required";
            public static final String TERMINAL_NAME_REQUIRED = "Terminal Name field required";
            public static final String TERMINAL_DOT_REQUIRED = "Terminal DOT field required";
            public static final String TERMINAL_ADDRESS_REQUIRED = "Terminal Address field required";
            public static final String TERMINAL_CONTACT_NUMBER_REQUIRED = "Terminal contact number field required";
            public static final String TERMINAL_TIMEZONE_REQUIRED = "Terminal Time zone field required";

            // Terminal SUCCESS Messages
            public static final String TERMINAL_CREATED = "Terminal added Successfully";
            public static final String TERMINAL_UPDATED = "Terminal updated successfully";
            public static final String TERMINAL_DELETED = "Terminal deleted successfully";

            //Tractor Error messages
            public static final String TRACTOR_LIST_NOT_FOUND = "Tractor list not found";
            public static final String TRACTOR_ID_NOT_FOUND = "Tractor ID not found";
            public static final String FAILED_TO_CREATE_TRACTOR = "Failed to create new tractor";
            public static final String FAILED_TO_UPDATE_TRACTOR = "Failed to update tractor";
            public static final String FAILED_TO_DELETE_TRACTOR = "Failed to delete tractor";
            public static final String TRACTOR_FIELDS_REQUIRED = "Tractor fields required";
            public static final String TRACTOR_NUMBER_FIELD_REQUIRED = "Tractor number field required";

            //Tractor Success messages
            public static final String TRACTOR_CREATED = "Tractor added successfully";
            public static final String TRACTOR_UPDATED = "Tractor updated successfully";
            public static final String TRACTOR_DELETED = "Tractor deleted successfully";

            //Tractor Error messages
            public static final String TRAILER_FIELDS_REQUIRED = "Trailer fields required";
            public static final String TRAILER_NUMBER_FIELD_REQUIRED = "Trailer number field required";
            public static final String TRAILER_ID_NOT_FOUND = "Trailer ID not found";
            public static final String TRAILER_LIST_NOT_FOUND = "Trailer list not found";
            public static final String FAILED_TO_ADD_TRAILER = "Failed to add trailer";
            public static final String FAILED_TO_UPDATE_TRAILER = "Failed to update trailer";
            public static final String FAILED_TO_DELETE_TRAILER = "Failed to delete trailer";

            //Tractor Success messages
            public static final String TRAILER_CREATED = "Trailer added successfully";
            public static final String TRAILER_UPDATED = "Trailer updated successfully";
            public static final String TRAILER_DELETED = "Trailer deleted successfully";

            //BOL Error messages
            public static final String NO_BoL_DATA_FOUND = "No BoL data found";

            //IFTA Success messages
            public static final String IFTA_CREATED = "IFTA Created";
            public static final String IFTA_DELETED = "IFTA Deleted";
            public static final String IFTA_UPDATED = "IFTA Updated";

            //IFTA Error messages
            public static final String FAILED_TO_CREATE_IFTA = "Ifta can not be stored in Driver Connect";
            public static final String IFTA_IMAGE_NOT_FOUND = "Image not found";
            public static final String INVALID_IFTA_DATA = "Invalid ifta data";
            public static final String IFTA_ALREADY_EXISTED = "Ifta already existed";
            public static final String IFTA_DETAILS_NOT_FOUND = "No IFTA Details found";

            //Subscription Error messages 
            public static final String SUBSCRIPTIONS_EXPIRED = "Your subscription has ended.";
            public static final String GRACE_SUBSCRIPTIONS_EXPIRED = "Grace period for your subscription has ended";

            //Messaging Success Messages
            public static final String OPENFIRE_USER_CREATED = "OPENFIRE USER CREATED";
            public static final String OPENFIRE_USER_PRESENT = "OPENFIRE USER PRESENT";
            public static final String MESSAGES_FOUND = "MESSAGES FOUND";

            //Messaging Error Messages
            public static final String OPENFIRE_USER_CREATE_FAIL = "CANNOT CREATE OPENFIRE USER";
            public static final String NO_MESSAGES_FOUND = "NO MESSASGES FOUND";
            
            //Timezone Success Codes
            public static final String TIMEZONE_DATA_FETCHED = "Timezone data fetched successfully";
            public static final String TIMEZONE_UPDATED = "Timezone updated successfully";
            
            //Timezone Error Codes
            public static final String FAILED_TO_FETCH_TIMEZONE_DATA = "Failed to fetch timezone data";
            public static final String FAILED_TO_UPDATED_TIMEZONE = "Failed to update timezone";
            
            //HoS Miles Success Codes
            public static final String HOS_MILES_DATA_FETCHED = "HoS miles data fetched successfully";
            public static final String HOS_MILES_UPDATED = "HoS miles updated successfully";
            
            //HoS Miles Error Codes
            public static final String FAILED_TO_FETCH_HOS_MILES_DATA = "Failed to fetch HoS miles data";
            public static final String FAILED_TO_UPDATED_HOS_MILES = "Failed to update HoS miles";
            
            //Driving Status success code
            public static final String FAULT_RECOVER_DRIVING_STATUS_CREATED = "Fault-Recovery driving status created successfully";
            public static final String FAULT_RECOVER_DRIVING_STATUS_FETCHED = "Fault-Recovery driving Status fetched successfully";
            
            //Driving Status error code
            public static final String FAILED_TO_CREATE_FAULT_RECOVER_DRIVING_STATUS = "Failed to create Fault-Recovery driving status";
            public static final String FAILED_TO_FETCH_FAULT_RECOVER_DRIVING_STATUS = "Failed to fecth Fault-Recovery driving status";
            //Manual HoS status success codes
            public static final String MANUAL_STATUS_DATA_UPDATED_SUCCESSFULLY = "Manual status data updated successfully";
            
            //Manual HoS status error codes
            public static final String FAILED_TO_UPDATE_MANUAL_STATUS_DATA = "Failed to update manual status data";
        }

        public interface RESPONSE_DESCRIPTION {

            public static final String FAILURE = "Something went wrong , unable to perform requested operation!";

            // DB RELATED ERROR
            public static final String TRANSACTION_EXCEPTION = "CannotCreateTransactionException";
            public static final String JDBC_EXCEPTION = "GenericJDBCException";
            public static final String SQL_SYNTAX_EXCEPTION = "SQLGrammarException";
            public static final String UNIQUE_KEY_EXCEPTION = "NonUniqueObjectException";
            public static final String CONSTRAINT_VIOLATION_EXCEPTION = "ConstraintViolationException";
            public static final String QUERY_EXCEPTION = "QueryException";
            public static final String NULL_POINTER_EXCEPTION = "NullPointerException";
            public static final String UNKNOWN_EXCEPTION = "Unknown exception or error";
        }

    }

    public interface STRING_CONSTANTS {

        public static final String CURRENT_DATE_TIME = "currentDateTime";
        public static final String EMPTY_STRING = "";
        public static final String STAR_SYMBOL = "*";
        public static final String QUESTION_MARK = "?";
        public static final String AMPERSAND_SYMBOL = "&";
        public static final String HYPHEN_SYMBOL = "-";
        public static final String COMMA = ",";

        public static final String ZERO = "0";
        public static final String DATA_SOURCE_APP = "DC-App";
        public static final String DVIR_TOTAL_DEFECTS_KEY = "total_defects";
       
        public static final String UNASSIGNED = "UNASSIGNED";
    }

    public interface USER_EVENT_CONSTANTS {

        public static final String LOGIN = "Login";
        public static final String LOGOUT = "Logout";
    }

    public interface INTEGER_CONSTANTS {

        public static final int ZERO = 0;
        public static final int ONE = 1;
        public static final int TWO = 2;
        public static final int THREE = 3;
        public static final int FOUR = 4;
        public static final int FIVE = 5;
        public static final int SIX = 6;
        public static final int SEVEN = 7;
        public static final int EIGHT = 8;
        public static final int FOURTEEN = 14;
        public static final int THIRTY = 30;
        public static final int NEGATIVE_ONE = -1;
        public static final int TWENTY = 20;
        public static final int NINE = 9;
        public static final int ELEVEN = 11;
        public static final int TWELVE = 12;
        public static final int TEN = 10;

    }

    public interface SUBSCRIPTION_TYPES {

        public static final String FREE_TRIAL = "Free Trial";
        public static final String BASIC = "Basic";
        public static final String COMPLIANCE_TRIAL = "Compliance Trial";
        public static final String COMPLIANCE = "Compliance";
        public static final String CORE = "Core";
        public static final String COMMERCIAL = "Commercial";
    }

    public interface SUBSCRIPTION_PLAN_LEVEL {

        public static final String L1 = "L1";
        public static final String L2 = "L2";
        public static final String L3 = "CL-03";
        public static final String L4 = "CL-04";
        public static final String L5 = "CL-05";
        public static final String L6 = "CL-06";
    }

    public interface SUBSCRIPTION_STATUS {

        public static final String ACTIVE = "Active";
        public static final String EXPIRED = "Expired";

    }

    public interface LAT_LONG_CONSTANTS {

        public static final double MAX_VALID_LAT_VALUE = 90;
        public static final double MIN_VALID_LAT_VALUE = -90;
        public static final double MAX_VALID_LONG_VALUE = 180;
        public static final double MIN_VALID_LONG_VALUE = -180;
        public static final double DEFAULT_LAT_LONG_VALUE = 200;
    }

    public interface EVENTS {

        public static final String ODOMETER = "odometer";
        public static final String SPEED = "speed";
        public static final String ENGINE_RPM = "engineRPM";
    }

    public interface SERVER_IP {

        public static final String STAGING_SERVER = "http://114.143.225.130";
        public static final String PRODUCTION_SERVER = "https://driverconnect.randmcnally.com";
        public static final String HTTP_FILE_SERVER = "https://s3.amazonaws.com/driverconnect/";

    }

    public interface DIRECTORY_PATH {

        public String ELD50_FIRMWARE = "dc/eld50/";
        public String ELD75_FIRMWARE = "dc/eld75/";
        public String DC200_FIRMWARE = "dc/dc200/";
    }

    // for email verification code
    public enum VerificationCode {

        USER("User"),
        COMPANY("Company"),
        USER_COMPANY("User_Company");

        private final String verificationCode;

        private VerificationCode(final String verificationCode) {
            this.verificationCode = verificationCode;
        }

        @Override
        public String toString() {
            return verificationCode;
        }
    }

    //FORGOT_INFO_ACTION CONSTANTS
    public interface FORGOT_INFO_ACTION {

        public static final String PASSWORD = "Password";
        public static final String COMPANY_INFO = "CompanyInfo";
    }

    public interface EMAILCONSTANTS {

        public static final String CHAR_ENCODING_UTF_8 = "utf-8";
        public static final String CompanySubscription_TEMPLATE = "mailTemplate/CompanySubscriptionMail.vm";
        public static final String EmailOnUserReg_TEMPLATE = "mailTemplate/EmailOn_UserReg.html";
        public static final String EmailOnDongleReg_TEMPLATE = "mailTemplate/EmailOn_DongleReg.html";
        public static final String EmailOnForgotCompanyInfo_TEMPLATE = "mailTemplate/EmailOn_ForgotCompanyInfo.html";
        public static final String EmailOnForgotPwd_TEMPLATE = "mailTemplate/EmailOn_ForgotPwd.html";
        public static final String EmailOnVerification_TEMPLATE = "mailTemplate/EmailOn_Verification.html";
        public static final String EmailOnVerificationLink_TEMPLATE = "mailTemplate/EmailOn_VerificationLink.html";
        public static final String EmailOnEventGeneration_TEMPLATE = "mailTemplate/EmailOn_event_notification.html";
        public static final String EmailOnInspectionReport_TEMPLATE = "mailTemplate/EmailOn_inspection_report.html";

        public static final String RM_EMAIL_ID = "no-reply@randmcnally.com";

    }

    public interface LOGGER_CONSTANT {

        public static final String RESPONSE_SENT = "Response Sent: [ ";
        public static final String DEVICE_NOT_FOUND = "Device not found";
        public static final String ERROR_OCCURRED = "Error occurred: ";
        public static final String SUCCESS = "Success";
        public static final String ERROR = " Error ";
        public static final String USER_NOT_AUTHORIZED = "User not authorized";
        public static final String VALID_DATA_FOUND = "Vaild Data found";
        public static final String EXCEPTION_OCCURED = "Exception Occurred: ";
        public static final String HOS_BY_USER = "getHoSByUser()";
        public static final String UPDATE_HoS_SERVICE_IMPL = "In update HoS Service IMPL returning from here -->";
        public static final String TIME_STAMP = "timeStamp : ";
        public static final String VALIDATE_CREATE_EVENT_REQUEST = "validateCreateHoSEventRequest --> duration in validator - ";
        public static final String EXCEPTION_IS = "exception is :";
        public static final String DATA_RECEIVED = "Data received: ";
        public static final String AUTHENTICATION_FAILED = "Authentication Failed";
        public static final String MALFORMED_DATA = "Malformed Data";
        public static final String USER_LOADED_SUCCESSFULLY = "User loaded successfully, User details=";
        public static final String COMPANY_LOADED_SUCCESSFULLY = "Company loaded successfully, Company details ==> companyId = ";
        public static final String API_DEPRECATED = "API is deprecated; please update your App";

    }

    public interface IFTA_CONFIGURABLE_PARAMETER {

        public int TIME_INTERVAL_FOR_STATEMILEAGE_CALCULATION = 150; // time interval in seconds to store data for state mileage
        public double COVERSION_FACTOR_FOR_LITRE_TO_GALLON = 0.26417;
        public int TIME_INTERVAL_FOR_STATECROSSING_CALCULATION = 150; // IMP: This should be in sync with TIME_INTERVAL_FOR_STATEMILEAGE_CALCULATION
        public double COVERSION_FACTOR_FOR_KM_TO_MILES = 0.62137;
        public double AVERAGE_MILEAGE = 6.0;
        public double AVERAGE_MILEAGE_KMPL = 2.55086;
        public static final long MAX_ODOMETER = 20000000L;
        public static final int MAX_DISTANCE_TRAVELLED_SM_INTERVAL = 12; //This is the max distance travelled in 150 seconds considering speed = 240 kmph
        public long MAX_TIME_FOR_150_BLOCK = 600000; // IMP: This should be in sync with TIME_INTERVAL_FOR_STATEMILEAGE_CALCULATION
        //Events constant
        public static final double VEHICLE_ACCELERATION_THRESHOLD = 2.0;
        public static final double VEHICLE_DECELERATION_THRESHOLD = -2.0;
        public static final int TIME_INTERVAL_FOR_ACCELERATION_DECELERATION = 10;
        public static final int MIN_RPM_FOR_OVER_IDELING = 550;
        public static final int MAX_RPM_FOR_OVER_IDELING = 700;
        public static final int TIME_INTERVAL_FOR_OVER_IDELING = 600;
        public static final int VEHICLE_START_SPEED_LIMIT = 8;  //kmph
        public static final int ACC_DEC_EVENT_TIME_DIFF = 5;  //kmph
        public static final int DEFAULT_MAX_SPEED = 180;
        
        //UserEventNameId Constants
        public static final int EVENT_ID_ONE = 1; //Login
        public static final int EVENT_ID_TWO = 2; // Logout
        public static final int EVENT_ID_THREE = 3; //Ignition On
        public static final int EVENT_ID_FOUR = 4; //Ignition Off
        public static final int EVENT_ID_FIVE = 5; //Vehicle Start
        public static final int EVENT_ID_SIX = 6;  // Vehicle Stop
        public static final int EVENT_ID_SEVEN = 7; // Over Idling
        public static final int EVENT_ID_EIGHT = 8;//Sudden Acceleration
        public static final int EVENT_ID_NINE = 9; //Sudden Deceleration
        public static final int EVENT_ID_TEN = 10; // HosDuty Change
        public static final int EVENT_ID_ELEVEN = 11; //HoS Update

    }

    public interface TRACK_N_TRACE_CONFIGURABLE_PARAMETER {

        public int TIME_INTERVAL_FOR_TRACK_N_TRACE = 300;
    }
    
    public enum EVENT_NAME {
//        Login(1),
//        Logout(2),
//        IgnitionOn(3),
//        IgnitionOff(4),
//        VehicleStart(5),
//        VehicleStop(6),
        OverIdling(7),  //calls constructor with value 3
        SuddenAcceleration(8),  //calls constructor with value 2
        SuddenDeceleration(9),  //calls constructor with value 3
        Geofence(10),  //calls constructor with value 2
        Speeding(11),  //calls constructor with value 3
        HardBraking(12),  //calls constructor with value 2
//        Driving(13),
//        OnDuty(14),
//        OffDuty(15),
//        SleeperBerth(16)
        PostedSpeedLimit(17)
        ; // semicolon needed when fields / methods follow

        private final int eventNameId;

        EVENT_NAME(int eventNameId) {
            this.eventNameId = eventNameId;
        }

        public int getEventNameId() {
            return this.eventNameId;
        }
    }
    
    public interface JasperConstants{
        public static final long ELD_ACC_MILE_MIN = 0;
        public static final long ELD_ACC_MILE_MAX = 9999;
        public static final double ELD_ENG_HOURS_MIN = 0.0;
        public static final double ELD_ENG_HOURS_MAX = 99.90;

    }
    public enum UnassignedDrivingTypeEnum {
	unassigned, codriver
    }

	// Added for audit fields capture task
	public interface AUDIT_STRING {

		public static final String AUDIT_ASYNC_START = "Audit Async process started.";
		public static final String AUDIT_ASYNC_END = "Audit Async process completed.";
		public static final String AUDIT_ASYNC_DURATION = "Audit Async Duration: ";
		public static final String AUDIT_UPDATE_START = "Audit table update start.";
		public static final String AUDIT_UPDATE_END = "Audit table update completed.";
	}

	// Added for audit fields capture task
	public interface AUDIT_CHECK {
		public static final String ALL_TABLE = "0";
		public static final String USER_TABLE = "1";
	}

	// below interface to express different client
	// 1 - DriverConnect, 2 - Web Service (DCWS), 3 - Web Service (DCWS_Cust), 4 - Warehouse, 5 - Web Service (OwnerOperatorWS)
	public interface CALLING_CLIENT {
		public static final int CLIENT = 2;
	}

}
