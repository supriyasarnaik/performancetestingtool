/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usercreation;

/**
 *
 * @author ssarnaik
 */
public class HosResponse {
    private UpdateHoSResponse[] response;

    private String subscriptionStatus;

    public UpdateHoSResponse[] getResponse ()
    {
        return response;
    }

    public void setResponse (UpdateHoSResponse[] response)
    {
        this.response = response;
    }

    public String getSubscriptionStatus ()
    {
        return subscriptionStatus;
    }

    public void setSubscriptionStatus (String subscriptionStatus)
    {
        this.subscriptionStatus = subscriptionStatus;
    }

    @Override
    public String toString()
    {
        return "Hos [response = "+response+", subscriptionStatus = "+subscriptionStatus+"]";
    }
}
