package usercreation;

public class HoSViolations {
    private int violationId;
    private int violationHoSId;
    private int violationTypeId;
    private int violationCode;
    private long violationCreatedDate;
    private String hosViolationCreatedDateTime;
    private String violationTypeDescription;
    private double violationLatitude;
    private double violationLongitude;
    private String violationLocation;
    private short status;

    public int getViolationId() {
        return violationId;
    }

    public void setViolationId(int violationId) {
        this.violationId = violationId;
    }

    public int getViolationHoSId() {
        return violationHoSId;
    }

    public void setViolationHoSId(int violationHoSId) {
        this.violationHoSId = violationHoSId;
    }

    public int getViolationTypeId() {
        return violationTypeId;
    }

    public void setViolationTypeId(int violationTypeId) {
        this.violationTypeId = violationTypeId;
    }

    public int getViolationCode() {
        return violationCode;
    }

    public void setViolationCode(int violationCode) {
        this.violationCode = violationCode;
    }

    public long getViolationCreatedDate() {
        return violationCreatedDate;
    }

    public void setViolationCreatedDate(long violationCreatedDate) {
        this.violationCreatedDate = violationCreatedDate;
    }

    public String getHosViolationCreatedDateTime() {
        return hosViolationCreatedDateTime;
    }

    public void setHosViolationCreatedDateTime(String hosViolationCreatedDateTime) {
        this.hosViolationCreatedDateTime = hosViolationCreatedDateTime;
    }

    public String getViolationTypeDescription() {
        return violationTypeDescription;
    }

    public void setViolationTypeDescription(String violationTypeDescription) {
        this.violationTypeDescription = violationTypeDescription;
    }

    public double getViolationLatitude() {
        return violationLatitude;
    }

    public void setViolationLatitude(double violationLatitude) {
        this.violationLatitude = violationLatitude;
    }

    public double getViolationLongitude() {
        return violationLongitude;
    }

    public void setViolationLongitude(double violationLongitude) {
        this.violationLongitude = violationLongitude;
    }

    public String getViolationLocation() {
        return violationLocation;
    }

    public void setViolationLocation(String violationLocation) {
        this.violationLocation = violationLocation;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Violations Bean [ violationId = " + violationId
                + ", violationHoSId = " + violationHoSId
                + ", violationTypeId = " + violationTypeId
                + ", violationCode = " + violationCode
                + ", violationCreatedDate = " + violationCreatedDate
                + ", violationTypeDescription = " + violationTypeDescription
                + ", violationLatitude = " + violationLatitude
                + ", violationLongitude = " + violationLongitude
                + ", violationLocation = " + violationLocation
                + ", status = " + status + "]";
    }
    
}
