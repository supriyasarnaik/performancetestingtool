package usercreation;

import java.util.ArrayList;
import java.util.List;

public class UpdateHoSRequestV1 {

    ArrayList<ApiHoSV1> hosLog;

    String userAccessToken;
    String currentStatus;
    double currentLatitude;
    double currentLongitude;
    RuleSetInfo ruleSetInfo;

    private long ymCrossedTime;

    private long pcCrossedTime;
    
    private int currentStatusStartSpeed;

    public RuleSetInfo getRuleSetInfo() {
        return ruleSetInfo;
    }

    public void setRuleSetInfo(RuleSetInfo ruleSetInfo) {
        this.ruleSetInfo = ruleSetInfo;
    }

    public List<ApiHoSV1> getHosLog() {
        return hosLog;
    }

    public void setHosLog(List<ApiHoSV1> hosLog) {
        this.hosLog = (ArrayList<ApiHoSV1>) hosLog;
    }

    public String getUserAccessToken() {
        return userAccessToken;
    }

    public void setUserAccessToken(String userAccessToken) {
        this.userAccessToken = userAccessToken;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public double getCurrentLatitude() {
        return currentLatitude;
    }

    public void setCurrentLatitude(double currentLatitude) {
        this.currentLatitude = currentLatitude;
    }

    public double getCurrentLongitude() {
        return currentLongitude;
    }

    public void setCurrentLongitude(double currentLongitude) {
        this.currentLongitude = currentLongitude;
    }

    public long getYmCrossedTime() {
        return ymCrossedTime;
    }

    public void setYmCrossedTime(long ymCrossedTime) {
        this.ymCrossedTime = ymCrossedTime;
    }

    public long getPcCrossedTime() {
        return pcCrossedTime;
    }

    public void setPcCrossedTime(long pcCrossedTime) {
        this.pcCrossedTime = pcCrossedTime;
    }

    public int getCurrentStatusStartSpeed() {
		return currentStatusStartSpeed;
	}

	public void setCurrentStatusStartSpeed(int currentStatusStartSpeed) {
		this.currentStatusStartSpeed = currentStatusStartSpeed;
	}

	@Override
    public String toString() {
        return "UpdateHoSRequest [hosLogSize = " + hosLog.size()
                + ", userAccessToken = " + userAccessToken
                + ", currentStatus = " + currentStatus + "]";
    }

}
