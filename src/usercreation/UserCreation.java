/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usercreation;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ssarnaik
 */
public class UserCreation {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        new UiDesign().setVisible(true);
    }

    public String GetLogin(String requestUrl, String userRegRequest) {
        StringBuffer response = null;
        try {
            String url = requestUrl;
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setRequestProperty("Content-Type", "application/json");

            // Sending post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(userRegRequest.toString());
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
            System.out.println("nSending 'POST' request to URL : " + url);
            System.out.println("Post Data : " + userRegRequest);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String output;
            response = new StringBuffer();

            while ((output = in.readLine()) != null) {
                response.append(output);
            }
            in.close();

            //printing result from response
            System.out.println(response.toString());
        } catch (MalformedURLException ex) {
            Logger.getLogger(UserCreation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserCreation.class.getName()).log(Level.SEVERE, null, ex);
        }

        return response.toString();

    }

    public String GetUpdateHos(String requestUrl, String userRegRequest) {
        StringBuffer response = null;
        try {
            String url = requestUrl;
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setRequestProperty("Content-Type", "application/json");

            // Sending post request
            con.setConnectTimeout(20000000);
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(userRegRequest.toString());
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
            System.out.println("nSending 'POST' request to URL : " + url);
            System.out.println("Post Data : " + userRegRequest);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String output;
            response = new StringBuffer();

            while ((output = in.readLine()) != null) {
                response.append(output);
            }
            in.close();

            //printing result from response
            System.out.println("*****************************" + response.toString());
        } catch (MalformedURLException ex) {
            Logger.getLogger(UserCreation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserCreation.class.getName()).log(Level.SEVERE, null, ex);
        }

        return response.toString();

    }

    public Connection getConnection(String url) {

        Connection conn = null;
        try {

            String myDriver = "com.mysql.jdbc.Driver";
            String myUrl = "jdbc:mysql://localhost:3306/rm_abc";
            if (url.equalsIgnoreCase("rm_performance")) {
                myUrl = "jdbc:mysql://driverconnecttest.cucxrj3bkwnv.us-east-1.rds.amazonaws.com:3306/rm_performance";

            } else if (url.equalsIgnoreCase("local")) {
                myUrl = "jdbc:mysql://localhost:3306/rm_abc";
            }

            Class.forName(myDriver);
//            conn = DriverManager.getConnection(myUrl, "root", "Rpr3XpaN");
            conn = DriverManager.getConnection(myUrl, "root", "root");
        } catch (Exception e) {
            System.err.println("Got an exception! " + e);
            e.printStackTrace();
        }
        return conn;
    }

    public String insertUserDetails(Connection conn, dbUserRequest userRequest) {
        try {
            String query1 = "insert into rm_user (user_first_name,user_last_name,user_contact_number,user_verified_email,"
                    + " user_contact_email,user_contact_email_is_verified,user_password,user_role_id,"
                    + " user_company_id,user_terminal_id,user_driver_login_id,user_hos_rule,user_tz,"
                    + "user_email_verification_validity,user_exemption_id,user_is_driver,user_access_token,user_is_logged_in) "
                    + " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement st1 = conn.prepareStatement(query1);
            st1.setString(1, userRequest.getUserFirstName());
            st1.setString(2, userRequest.getUserLastName());
            st1.setString(3, userRequest.getUserNumber());
            st1.setString(4, userRequest.getUserVerifiedEmail());
            st1.setString(5, userRequest.getUserEmail());
            st1.setInt(6, userRequest.getUserEmailIsVerified());
            st1.setString(7, userRequest.getUserPassword());
            st1.setInt(8, userRequest.getRole());
            st1.setInt(9, userRequest.getCompany());
            st1.setInt(10, userRequest.getUserTerminalId());
            st1.setString(11, userRequest.getUserDriverLoginId());
            st1.setString(12, userRequest.getUserHoSRule());
            st1.setString(13, userRequest.getUserTz());
            st1.setLong(14, userRequest.getUserEmailVerificationValidity());
            st1.setInt(15, userRequest.getUserExemptionId());
            st1.setInt(16, userRequest.getUserIsDriver());
            st1.setString(17, userRequest.getUserAccessToken());
            st1.setInt(18, userRequest.getUserIsLoggedIn());

            st1.execute();
            return "User Created Successfully";
        } catch (SQLException ex) {
            Logger.getLogger(UserCreation.class.getName()).log(Level.SEVERE, null, ex);
            return ex.toString();
        }

    }

    public String deleteUserDetails(Connection conn, String userFirstName, int companyId) {
        try {
            ResultSet rs = null;
            String countQuery = "select count(1) from rm_user where user_first_name ='" + userFirstName + "' AND user_company_id =" + companyId + ";";
            System.out.println("countQuery:: " + countQuery);
            PreparedStatement st1 = conn.prepareStatement(countQuery);
            rs = st1.executeQuery();

            int count = 0;
            while (rs.next()) {
                count++;
            }

            if (count > 0) {
                String query1 = "delete from rm_hos_events where hos_events_hos_id IN "
                        + "(select hos_id from rm_hos where hos_driver_id IN "
                        + "(select user_id from rm_user where user_first_name = '" + userFirstName + "' AND user_company_id =" + companyId + "));";

                System.out.println("query1: " + query1);
                st1 = conn.prepareStatement(query1);
                st1.execute();

                String query2 = "delete from rm_hos where hos_driver_id IN "
                        + "(select user_id from rm_user where user_first_name ='" + userFirstName + "' AND user_company_id =" + companyId + ");";

                System.out.println("query2: " + query2);
                st1 = conn.prepareStatement(query2);
                st1.execute();
                String query3 = "delete from rm_user where user_first_name ='" + userFirstName + "' AND user_company_id =" + companyId + ";";

                System.out.println("query3: " + query3);
                st1 = conn.prepareStatement(query3);
                st1.execute();
                return "Success";
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserCreation.class.getName()).log(Level.SEVERE, null, ex);
            return "Error";
        }
        return "Error";
    }

      
}
