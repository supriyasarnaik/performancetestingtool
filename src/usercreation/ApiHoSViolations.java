package usercreation;

public class ApiHoSViolations {

    private long createdDate;

    private int violationId;
    
    private String violationType;

    private double latitude;

    private double longitude;

    private String locationName;

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public int getViolationId() {
        return violationId;
    }

    public void setViolationId(int violationId) {
        this.violationId = violationId;
    }

    public String getViolationType() {
        return violationType;
    }

    public void setViolationType(String violationType) {
        this.violationType = violationType;
    }
    
    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public void createApiViolationObject(HoSViolations hosViolations) {
        this.setCreatedDate(hosViolations.getViolationCreatedDate());
        this.setViolationId(hosViolations.getViolationCode());
        this.setViolationType(hosViolations.getViolationTypeDescription());
        this.setLatitude(hosViolations.getViolationLatitude());
        this.setLongitude(hosViolations.getViolationLongitude());
        this.setLocationName(hosViolations.getViolationLocation());
    }

    @Override
    public String toString() {
        return "ApiHoSViolations [CreatedDate = " + createdDate
                + ", ViolationId = " + violationId
                + ", Latitude = " + latitude 
                + ", Longitude = " + longitude
                + ", LocationName = " + locationName+ "]";
    }
}
