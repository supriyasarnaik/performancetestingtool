package usercreation;


public class Company {

	private int companyId;
	private String companyCode;
	private String companyName;
	private String companyStreetAddress;
	private String companyCity;
	private String companyStateProvince;
	private String companyZipCode;
	private String companyCountry;
	private String companyDot;
	private String companyPhone;
	private CompanyContractType companyContractType;
	private String companyVerifiedEmail;
	private String companyContactEmail;
	private String companyTimeZone;
	private boolean companyIsDST;
	private long companyEmailVerificationValidity;
	private short companyEmailIsVerified;
	private double companyLatitude;
	private double companyLongitude;
	private short isELDEnabled;
	private short isMoaEnabled;
	private int moaGroupId;
	private boolean status;

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyStreetAddress() {
		return companyStreetAddress;
	}

	public void setCompanyStreetAddress(String companyStreetAddress) {
		this.companyStreetAddress = companyStreetAddress;
	}

	public String getCompanyCity() {
		return companyCity;
	}

	public void setCompanyCity(String companyCity) {
		this.companyCity = companyCity;
	}

	public String getCompanyStateProvince() {
		return companyStateProvince;
	}

	public void setCompanyStateProvince(String companyStateProvince) {
		this.companyStateProvince = companyStateProvince;
	}

	public String getCompanyZipCode() {
		return companyZipCode;
	}

	public void setCompanyZipCode(String companyZipCode) {
		this.companyZipCode = companyZipCode;
	}

	public String getCompanyCountry() {
		return companyCountry;
	}

	public void setCompanyCountry(String companyCountry) {
		this.companyCountry = companyCountry;
	}

	public String getCompanyDot() {
		return companyDot;
	}

	public void setCompanyDot(String companyDot) {
		this.companyDot = companyDot;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	public CompanyContractType getCompanyContractType() {
		return companyContractType;
	}

	public void setCompanyContractType(CompanyContractType companyContractType) {
		this.companyContractType = companyContractType;
	}

	public String getCompanyVerifiedEmail() {
		return companyVerifiedEmail;
	}

	public void setCompanyVerifiedEmail(String companyVerifiedEmail) {
		this.companyVerifiedEmail = companyVerifiedEmail;
	}

	public String getCompanyContactEmail() {
		return companyContactEmail;
	}

	public void setCompanyContactEmail(String companyContactEmail) {
		this.companyContactEmail = companyContactEmail;
	}

	public String getCompanyTimeZone() {
		return companyTimeZone;
	}

	public void setCompanyTimeZone(String companyTimeZone) {
		this.companyTimeZone = companyTimeZone;
	}

	public boolean isCompanyIsDST() {
		return companyIsDST;
	}

	public void setCompanyIsDST(boolean companyIsDST) {
		this.companyIsDST = companyIsDST;
	}

	public long getCompanyEmailVerificationValidity() {
		return companyEmailVerificationValidity;
	}

	public void setCompanyEmailVerificationValidity(long companyEmailVerificationValidity) {
		this.companyEmailVerificationValidity = companyEmailVerificationValidity;
	}

	public short getCompanyEmailIsVerified() {
		return companyEmailIsVerified;
	}

	public void setCompanyEmailIsVerified(short companyEmailIsVerified) {
		this.companyEmailIsVerified = companyEmailIsVerified;
	}

	public boolean isStatus() {
		return status;
	}

	public double getCompanyLatitude() {
		return companyLatitude;
	}

	public void setCompanyLatitude(double companyLatitude) {
		this.companyLatitude = companyLatitude;
	}

	public double getCompanyLongitude() {
		return companyLongitude;
	}

	public void setCompanyLongitude(double companyLongitude) {
		this.companyLongitude = companyLongitude;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public short getIsELDEnabled() {
		return isELDEnabled;
	}

	public void setIsELDEnabled(short isELDEnabled) {
		this.isELDEnabled = isELDEnabled;
	}
	
	public short getIsMoaEnabled() {
		return isMoaEnabled;
	}

	public void setIsMoaEnabled(short isMoaEnabled) {
		this.isMoaEnabled = isMoaEnabled;
	}

	public int getMoaGroupId() {
		return moaGroupId;
	}

	public void setMoaGroupId(int moaGroupId) {
		this.moaGroupId = moaGroupId;
	}

	@Override
	public String toString() {
		return "CompanyBean [companyId = " + companyId + ", companyCode = " + companyCode + ", companyName = " + companyName
				+ ", companyName = " + companyStreetAddress + ", companyCity = " + companyCity
				+ ", companyState = " + companyStateProvince + ", companyZip = " + companyZipCode
				+ ", companyCountry = " + companyCountry + ", companyDOT = " + companyDot
				+ ", companyContactNumber = " + companyPhone + ",companyContactEmail = " + companyContactEmail + ", companyTimezone = "
				+ companyTimeZone + "]";
	}
}
