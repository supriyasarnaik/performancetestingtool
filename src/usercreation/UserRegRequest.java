package usercreation;

public class UserRegRequest {
 private String userFirstName;
    private String userLastName;

    private String userEmail;

    private String userDriverLoginId;

    private String userPassword;
    
    private CompanyReqRequest cDet;

    public UserRegRequest() {
        super();
    }

    public UserRegRequest(String userFirstName, String userLastName,
            String userEmail, String userDriverLoginId, String userPassword) {
        super();
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
        this.userEmail = userEmail;
        this.userDriverLoginId = userDriverLoginId;
        this.userPassword = userPassword;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserDriverLoginId() {
        return userDriverLoginId;
    }

    public void setUserDriverLoginId(String userDriverLoginId) {
        this.userDriverLoginId = userDriverLoginId;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public CompanyReqRequest getcDet() {
        return cDet;
    }

    public void setcDet(CompanyReqRequest cDet) {
        this.cDet = cDet;
    }

    @Override
    public String toString() {
        return "UserRegRequest{" + "userFirstName=" + userFirstName + ", userLastName=" + userLastName + ", userEmail=" + userEmail + ", userDriverLoginId=" + userDriverLoginId + ", userPassword=" + userPassword + ", cDet=" + cDet + '}';
    }
    
    

}
