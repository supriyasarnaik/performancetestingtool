/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usercreation;


public class Device {
    private int deviceId;

    private String deviceSerialNumber;

    private String deviceIMEI;

    private String deviceBluetoothMacAddress;
    private String deviceWifiMacAddress;

    private String deviceMode;

    private String deviceSimNumber;
    
    private String deviceIccId;

    private Company company;
    private String deviceType;

    private String deviceFirmwareVersion;
    
    private String deviceName;

    private String deviceDescription;
    
    private long deviceRegisteredTime;

    private boolean status;

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceSerialNumber() {
        return deviceSerialNumber;
    }

    public void setDeviceSerialNumber(String deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }

    public String getDeviceIMEI() {
        return deviceIMEI;
    }

    public void setDeviceIMEI(String deviceIMEI) {
        this.deviceIMEI = deviceIMEI;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceFirmwareVersion() {
        return deviceFirmwareVersion;
    }

    public void setDeviceFirmwareVersion(String deviceFirmwareVersion) {
        this.deviceFirmwareVersion = deviceFirmwareVersion;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceDescription() {
        return deviceDescription;
    }

    public void setDeviceDescription(String deviceDescription) {
        this.deviceDescription = deviceDescription;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getDeviceBluetoothMacAddress() {
        return deviceBluetoothMacAddress;
    }

    public void setDeviceBluetoothMacAddress(String deviceBluetoothMacAddress) {
        this.deviceBluetoothMacAddress = deviceBluetoothMacAddress;
    }

    public String getDeviceWifiMacAddress() {
        return deviceWifiMacAddress;
    }

    public void setDeviceWifiMacAddress(String deviceWifiMacAddress) {
        this.deviceWifiMacAddress = deviceWifiMacAddress;
    }

    public String getDeviceMode() {
        return deviceMode;
    }

    public String getDeviceSimNumber() {
        return deviceSimNumber;
    }

    public void setDeviceSimNumber(String deviceSimNumber) {
        this.deviceSimNumber = deviceSimNumber;
    }

    public String getDeviceIccId() {
        return deviceIccId;
    }

    public void setDeviceIccId(String deviceIccId) {
        this.deviceIccId = deviceIccId;
    }
    
    public void setDeviceMode(String deviceMode) {
        this.deviceMode = deviceMode;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public long getDeviceRegisteredTime() {
        return deviceRegisteredTime;
    }

    public void setDeviceRegisteredTime(long deviceRegisteredTime) {
        this.deviceRegisteredTime = deviceRegisteredTime;
    }
    
    @Override
    public String toString() {
        return "DeviceBean [deviceId =" + deviceId + ", deviceSerialNumber =" + deviceSerialNumber
                + ", deviceIMEI =" + deviceIMEI + ", deviceType =" + deviceType
                + ", deviceName=" + deviceName + ", deviceDescription ="
                + deviceDescription + ", deviceBluetoothMacAddress=" + deviceBluetoothMacAddress + ", status=" + status + "]";
    }

}
