/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usercreation;

public class UpdateHoSResponse {

    private int code;

    private String type;

    private String message;

    private int serverId;

    private int clientId;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    @Override
    public String toString() {
        return "UpdateHoSResponse [" 
                + "code=" + code 
                + ", type=" + type 
                + ", message=" + message 
                + ", serverId=" + serverId 
                + ", clientId=" + clientId 
                + "]";
    }

    

    public UpdateHoSResponse() {
        super();
    }

//    public void createUpdateHoSResponseObject(CommonAPIResponse commonApiResponse, int serverId) {
//        this.setCode(commonApiResponse.getCode());
//        this.setMessage(commonApiResponse.getMessage());
//        this.setType(commonApiResponse.getType());
//        this.setServerId(serverId);
//    }
//    
//    public void createUpdateHoSResponseObject(int clientId, CommonAPIResponse commonApiResponse) {
//        this.setCode(commonApiResponse.getCode());
//        this.setMessage(commonApiResponse.getMessage());
//        this.setType(commonApiResponse.getType());
//        this.setClientId(clientId);
//    }
//    
//    public void createUpdateHoSResponseObject(CommonAPIResponse commonApiResponse, int serverId, int clientId) {
//        this.setCode(commonApiResponse.getCode());
//        this.setMessage(commonApiResponse.getMessage());
//        this.setType(commonApiResponse.getType());
//        this.setClientId(clientId);
//        this.setServerId(serverId);
//    }
//    
    public UpdateHoSResponse(int code, String type, String message) {
        super();
        this.code = code;
        this.type = type;
        this.message = message;
    }

}
