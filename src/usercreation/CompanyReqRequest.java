/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usercreation;

public class CompanyReqRequest {
    
    private String cName;
	private String cCountry;
    private String cAdd;
    
    private String cCity;
    
    private String cState;
    
    private String cZip;
    private String cTel;
    
    private String cDOT;

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public String getcAdd() {
        return cAdd;
    }

    public void setcAdd(String cAdd) {
        this.cAdd = cAdd;
    }

    public String getcCity() {
        return cCity;
    }

    public void setcCity(String cCity) {
        this.cCity = cCity;
    }

    public String getcState() {
        return cState;
    }

    public void setcState(String cState) {
        this.cState = cState;
    }

    public String getcZip() {
        return cZip;
    }

    public void setcZip(String cZip) {
        this.cZip = cZip;
    }

    public String getcTel() {
        return cTel;
    }

    public void setcTel(String cTel) {
        this.cTel = cTel;
    }

    public String getcDOT() {
        return cDOT;
    }

    public void setcDOT(String cDOT) {
        this.cDOT = cDOT;
    }

    public String getcCountry() {
		return cCountry;
	}

	public void setcCountry(String cCountry) {
		this.cCountry = cCountry;
	}

	@Override
    public String toString() {
        return "CompanyReqRequest{" 
                + "cName=" + cName 
                + ", cAdd=" + cAdd 
                + ", cCity=" + cCity 
                + ", cState=" + cState 
                + ", cZip=" + cZip 
                + ", cTel=" + cTel 
                + ", cDOT=" + cDOT 
                + '}';
    }
    
    
    
    
}
