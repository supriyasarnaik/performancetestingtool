package usercreation;
public class HoS {

	private int hosId;

	private long hosCreatedDate;

	private String hosCspEmailId;

	private User hosDriver;
	private int hosCompanyId;

	private String hosCreateDateTime;

	private boolean hosIsLogCertified;

	private int hosOnDutyTime;

	private int hosOffDutyTime;

	private int hosDrivingTime;

	private int hosSleeperBerthTime;

	private String hosCurrentStatus;

	private long hosRuleSetChangedDate;

	private double hosTripDistance;

	private int totalHosViolations;

	private String hosCertifiedBy;

	private double hosCurrentLatitude;

	private double hosCurrentLongitude;

	private int hosCurrentSpeed;

	private String hosCurrentRuleSet;

	private short hosCurrentIs100air;

	private short hosCurrentIsAdverse;

	private int hosCurrentExemptionId;

	private int hosCurrentSubstatusId;

	private long hosPCCrossedTime;

	private long hosymCrossedTime;

	private String hosTimeZone;

	private int hosDisplayStatus;

	private String hosDOTNumber;

	private boolean status;

	public int getHosId() {
		return hosId;
	}

	public void setHosId(int hosId) {
		this.hosId = hosId;
	}

	public long getHosCreatedDate() {
		return hosCreatedDate;
	}

	public void setHosCreatedDate(long hosCreatedDate) {
		this.hosCreatedDate = hosCreatedDate;
	}


	public void setHosCspEmailId(String hosCspEmailId) {
		this.hosCspEmailId = hosCspEmailId;
	}

	public User getHosDriver() {
		return hosDriver;
	}
			public String getHosCreateDateTime() {
		return hosCreateDateTime;
	}

	public void setHosCreateDateTime(String hosCreateDateTime) {
		this.hosCreateDateTime = hosCreateDateTime;
	}

	public String getHosCspEmailId() {
		return hosCspEmailId;
	}

	public void setHosDriver(User hosDriver) {
		this.hosDriver = hosDriver;
	}

	public int getHosCompanyId() {
		return hosCompanyId;
	}

	public void setHosCompanyId(int hosCompanyId) {
		this.hosCompanyId = hosCompanyId;
	}

	public boolean isHosIsLogCertified() {
		return hosIsLogCertified;
	}

	public void setHosIsLogCertified(boolean hosIsLogCertified) {
		this.hosIsLogCertified = hosIsLogCertified;
	}

	public int getHosOnDutyTime() {
		return hosOnDutyTime;
	}

	public void setHosOnDutyTime(int hosOnDutyTime) {
		this.hosOnDutyTime = hosOnDutyTime;
	}

	public int getHosOffDutyTime() {
		return hosOffDutyTime;
	}

	public void setHosOffDutyTime(int hosOffDutyTime) {
		this.hosOffDutyTime = hosOffDutyTime;
	}

	public int getHosDrivingTime() {
		return hosDrivingTime;
	}

	public void setHosDrivingTime(int hosDrivingTime) {
		this.hosDrivingTime = hosDrivingTime;
	}

	public int getHosSleeperBerthTime() {
		return hosSleeperBerthTime;
	}

	public void setHosSleeperBerthTime(int hosSleeperBerthTime) {
		this.hosSleeperBerthTime = hosSleeperBerthTime;
	}

	public String getHosCurrentStatus() {
		return hosCurrentStatus;
	}

	public void setHosCurrentStatus(String hosCurrentStatus) {
		this.hosCurrentStatus = hosCurrentStatus;
	}

	public long getHosRuleSetChangedDate() {
		return hosRuleSetChangedDate;
	}

	public void setHosRuleSetChangedDate(long hosRuleSetChangedDate) {
		this.hosRuleSetChangedDate = hosRuleSetChangedDate;
	}

	public double getHosTripDistance() {
		return hosTripDistance;
	}

	public void setHosTripDistance(double hosTripDistance) {
		this.hosTripDistance = hosTripDistance;
	}

	public int getTotalHosViolations() {
		return totalHosViolations;
	}

	public void setTotalHosViolations(int totalHosViolations) {
		this.totalHosViolations = totalHosViolations;
	}

	public String getHosCertifiedBy() {
		return hosCertifiedBy;
	}

	public void setHosCertifiedBy(String hosCertifiedBy) {
		this.hosCertifiedBy = hosCertifiedBy;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public double getHosCurrentLatitude() {
		return hosCurrentLatitude;
	}

	public void setHosCurrentLatitude(double hosCurrentLatitude) {
		this.hosCurrentLatitude = hosCurrentLatitude;
	}

	public double getHosCurrentLongitude() {
		return hosCurrentLongitude;
	}

	public void setHosCurrentLongitude(double hosCurrentLongitude) {
		this.hosCurrentLongitude = hosCurrentLongitude;
	}

	public String getHosCurrentRuleSet() {
		return hosCurrentRuleSet;
	}

	public void setHosCurrentRuleSet(String hosCurrentRuleSet) {
		this.hosCurrentRuleSet = hosCurrentRuleSet;
	}

	public short isHosCurrentIs100air() {
		return hosCurrentIs100air;
	}

	public void setHosCurrentIs100air(short hosCurrentIs100air) {
		this.hosCurrentIs100air = hosCurrentIs100air;
	}

	public short isHosCurrentIsAdverse() {
		return hosCurrentIsAdverse;
	}

	public void setHosCurrentIsAdverse(short hosCurrentIsAdverse) {
		this.hosCurrentIsAdverse = hosCurrentIsAdverse;
	}

	public int getHosCurrentExemptionId() {
		return hosCurrentExemptionId;
	}

	public void setHosCurrentExemptionId(int hosCurrentExemptionId) {
		this.hosCurrentExemptionId = hosCurrentExemptionId;
	}

	public int getHosCurrentSubstatusId() {
		return hosCurrentSubstatusId;
	}

	public void setHosCurrentSubstatusId(int hosCurrentSubstatusId) {
		this.hosCurrentSubstatusId = hosCurrentSubstatusId;
	}

	public long getHosPCCrossedTime() {
		return hosPCCrossedTime;
	}

	public void setHosPCCrossedTime(long hosPCCrossedTime) {
		this.hosPCCrossedTime = hosPCCrossedTime;
	}

	public long getHosymCrossedTime() {
		return hosymCrossedTime;
	}

	public void setHosymCrossedTime(long hosymCrossedTime) {
		this.hosymCrossedTime = hosymCrossedTime;
	}

	public String getHosTimeZone() {
		return hosTimeZone;
	}

	public void setHosTimeZone(String hosTimeZone) {
		this.hosTimeZone = hosTimeZone;
	}

	public short getHosCurrentIs100air() {
		return hosCurrentIs100air;
	}

	public short getHosCurrentIsAdverse() {
		return hosCurrentIsAdverse;
	}

	public int getHosCurrentSpeed() {
		return hosCurrentSpeed;
	}

	public void setHosCurrentSpeed(int hosCurrentSpeed) {
		this.hosCurrentSpeed = hosCurrentSpeed;
	}

	public int getHosDisplayStatus() {
		return hosDisplayStatus;
	}

	public void setHosDisplayStatus(int hosDisplayStatus) {
		this.hosDisplayStatus = hosDisplayStatus;
	}

	public String getHosDOTNumber() {
		return hosDOTNumber;
	}

	public void setHosDOTNumber(String hosDOTNumber) {
		this.hosDOTNumber = hosDOTNumber;
	}

	@Override
	public String toString() {
		return "HoS [hosId=" + hosId + ", hosCreatedDate=" + hosCreatedDate + ", hosCspEmailId=" + hosCspEmailId
				+ ", hosDriver=" + hosDriver + ", hosCompanyId=" + hosCompanyId + ", hosIsLogCertified="
				+ hosIsLogCertified + ", hosOnDutyTime=" + hosOnDutyTime + ", hosOffDutyTime=" + hosOffDutyTime
				+ ", hosDrivingTime=" + hosDrivingTime + ", hosSleeperBerthTime=" + hosSleeperBerthTime
				+ ", hosCurrentStatus=" + hosCurrentStatus + ", hosRuleSetChangedDate=" + hosRuleSetChangedDate
				+ ", hosTripDistance=" + hosTripDistance + ", totalHosViolations=" + totalHosViolations
				+ ", hosCertifiedBy=" + hosCertifiedBy + ", hosCurrentLatitude=" + hosCurrentLatitude
				+ ", hosCurrentLongitude=" + hosCurrentLongitude + ", hosCurrentSpeed=" + hosCurrentSpeed
				+ ", hosCurrentRuleSet=" + hosCurrentRuleSet + ", hosCurrentIs100air=" + hosCurrentIs100air
				+ ", hosCurrentIsAdverse=" + hosCurrentIsAdverse + ", hosCurrentExemptionId=" + hosCurrentExemptionId
				+ ", hosCurrentSubstatusId=" + hosCurrentSubstatusId + ", hosPCCrossedTime=" + hosPCCrossedTime
				+ ", hosymCrossedTime=" + hosymCrossedTime + ", hosTimeZone=" + hosTimeZone + ", hosDisplayStatus="
				+ hosDisplayStatus + ", hosDOTNumber=" + hosDOTNumber + ", status=" + status + "]";
	}

}
