package usercreation;

public class HoSEvent {

    public enum HoSStatus {

        ON_DUTY("ON DUTY"),
        OFF_DUTY("OFF DUTY"),
        SLEEPER_BERTH("SLEEPER BERTH"),
        DRIVING("DRIVING");

        private final String hosStatus;

        private HoSStatus(final String hosStatus) {
            this.hosStatus = hosStatus;
        }

        @Override
        public String toString() {
            return hosStatus;
        }
    }

    private int hosEventId;

    private int hosEventHoSId;

    private String hosEventTractorNumber;

    private String hosEventTrailerNumber;

    private int hosEventCode;

    private String hosEventDutyStatus;

    private long hosEventOdometer;

    private long hosEventStartOdometer;

    private long hosEventStartTime;

    private long hosEventEndTime;
    
    private String hosEventStartDateTime;

    private String hosEventEndDateTime;

    private double hosEventDuration;

    private boolean hosEventTetheredToDongle;

    private double hosEventLatitude;

    private double hosEventLongitude;

    private String hosEventLocation;

    private int hosEventSpeed;

    private String hosEventVin;

    private String hosEventComments;

    private String hosEventRule;

    private short hosEventsIs100air;

    private short hosEventsIsAdverse;

    private int hosEventsExemptionId;

    private int hoseEventsSubstatusId;

    private String hosEventsTripName;

    private int hosEventsDrivingStartThreshold;

    private int hosEventsDrivingEndThreshold;

    private String hosEventDOTNumber;

    private short hosEventELog;

    private boolean status;

    public int getHosEventId() {
        return hosEventId;
    }

    public void setHosEventId(int hosEventId) {
        this.hosEventId = hosEventId;
    }

    public int getHosEventHoSId() {
        return hosEventHoSId;
    }

    public void setHosEventHoSId(int hosEventHoSId) {
        this.hosEventHoSId = hosEventHoSId;
    }

    public String getHosEventTractorNumber() {
        return hosEventTractorNumber;
    }

    public void setHosEventTractorNumber(String hosEventTractorNumber) {
        this.hosEventTractorNumber = hosEventTractorNumber;
    }

    public String getHosEventTrailerNumber() {
        return hosEventTrailerNumber;
    }

    public void setHosEventTrailerNumber(String hosEventTrailerNumber) {
        this.hosEventTrailerNumber = hosEventTrailerNumber;
    }

    public int getHosEventCode() {
        return hosEventCode;
    }

    public void setHosEventCode(int hosEventCode) {
        this.hosEventCode = hosEventCode;
    }

    public String getHosEventDutyStatus() {
        return hosEventDutyStatus;
    }

    public void setHosEventDutyStatus(String hosEventDutyStatus) {
        this.hosEventDutyStatus = hosEventDutyStatus;
    }

    public long getHosEventOdometer() {
        return hosEventOdometer;
    }

    public void setHosEventOdometer(long hosEventOdometer) {
        this.hosEventOdometer = hosEventOdometer;
    }

    public long getHosEventStartTime() {
        return hosEventStartTime;
    }

    public void setHosEventStartTime(long hosEventStartTime) {
        this.hosEventStartTime = hosEventStartTime;
    }

    public long getHosEventEndTime() {
        return hosEventEndTime;
    }

    public String getHosEventStartDateTime() {
        return hosEventStartDateTime;
    }

    public void setHosEventStartDateTime(String hosEventStartDateTime) {
        this.hosEventStartDateTime = hosEventStartDateTime;
    }

    public String getHosEventEndDateTime() {
        return hosEventEndDateTime;
    }

    public void setHosEventEndDateTime(String hosEventEndDateTime) {
        this.hosEventEndDateTime = hosEventEndDateTime;
    }

    public double getHosEventDuration() {
        return hosEventDuration;
    }

    public void setHosEventDuration(double hosEventDuration) {
        this.hosEventDuration = hosEventDuration;
    }

    public void setHosEventEndTime(long hosEventEndTime) {
        this.hosEventEndTime = hosEventEndTime;
    }

    public boolean isHosEventTetheredToDongle() {
        return hosEventTetheredToDongle;
    }

    public void setHosEventTetheredToDongle(boolean hosEventTetheredToDongle) {
        this.hosEventTetheredToDongle = hosEventTetheredToDongle;
    }

    public double getHosEventLatitude() {
        return hosEventLatitude;
    }

    public void setHosEventLatitude(double hosEventLatitude) {
        this.hosEventLatitude = hosEventLatitude;
    }

    public double getHosEventLongitude() {
        return hosEventLongitude;
    }

    public void setHosEventLongitude(double hosEventLongitude) {
        this.hosEventLongitude = hosEventLongitude;
    }

    public String getHosEventLocation() {
        return hosEventLocation;
    }

    public void setHosEventLocation(String hosEventLocation) {
        this.hosEventLocation = hosEventLocation;
    }

    public int getHosEventSpeed() {
        return hosEventSpeed;
    }

    public void setHosEventSpeed(int hosEventSpeed) {
        this.hosEventSpeed = hosEventSpeed;
    }

    public String getHosEventVin() {
        return hosEventVin;
    }

    public void setHosEventVin(String hosEventVin) {
        this.hosEventVin = hosEventVin;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getHosEventComments() {
        return hosEventComments;
    }

    public void setHosEventComments(String hosEventComments) {
        this.hosEventComments = hosEventComments;
    }

    public String getHosEventRule() {
        return hosEventRule;
    }

    public void setHosEventRule(String hosEventRule) {
        this.hosEventRule = hosEventRule;
    }

    public short getHosEventsIs100air() {
        return hosEventsIs100air;
    }

    public void setHosEventsIs100air(short hosEventsIs100air) {
        this.hosEventsIs100air = hosEventsIs100air;
    }

    public short getHosEventsIsAdverse() {
        return hosEventsIsAdverse;
    }

    public void setHosEventsIsAdverse(short hosEventsIsAdverse) {
        this.hosEventsIsAdverse = hosEventsIsAdverse;
    }

    public int getHosEventsExemptionId() {
        return hosEventsExemptionId;
    }

    public void setHosEventsExemptionId(int hosEventsExemptionId) {
        this.hosEventsExemptionId = hosEventsExemptionId;
    }

    public int getHoseEventsSubstatusId() {
        return hoseEventsSubstatusId;
    }

    public void setHoseEventsSubstatusId(int hoseEventsSubstatusId) {
        this.hoseEventsSubstatusId = hoseEventsSubstatusId;
    }

    public long getHosEventStartOdometer() {
        return hosEventStartOdometer;
    }

    public void setHosEventStartOdometer(long hosEventStartOdometer) {
        this.hosEventStartOdometer = hosEventStartOdometer;
    }

    public String getHosEventsTripName() {
        return hosEventsTripName;
    }

    public void setHosEventsTripName(String hosEventsTripName) {
        this.hosEventsTripName = hosEventsTripName;
    }

    public int getHosEventsDrivingStartThreshold() {
        return hosEventsDrivingStartThreshold;
    }

    public void setHosEventsDrivingStartThreshold(int hosEventsDrivingStartThreshold) {
        this.hosEventsDrivingStartThreshold = hosEventsDrivingStartThreshold;
    }

    public int getHosEventsDrivingEndThreshold() {
        return hosEventsDrivingEndThreshold;
    }

    public void setHosEventsDrivingEndThreshold(int hosEventsDrivingEndThreshold) {
        this.hosEventsDrivingEndThreshold = hosEventsDrivingEndThreshold;
    }

    public String getHosEventDOTNumber() {
        return hosEventDOTNumber;
    }

    public void setHosEventDOTNumber(String hosEventDOTNumber) {
        this.hosEventDOTNumber = hosEventDOTNumber;
    }


    public short getHosEventELog() {
        return hosEventELog;
    }

    public void setHosEventELog(short hosEventELog) {
        this.hosEventELog = hosEventELog;
    }

    @Override
    public String toString() {
        return "HoSEvent [hosEventId=" + hosEventId + ", hosEventHoSId=" + hosEventHoSId + ", hosEventTractorNumber="
                + hosEventTractorNumber + ", hosEventTrailerNumber=" + hosEventTrailerNumber + ", hosEventCode="
                + hosEventCode + ", hosEventDutyStatus=" + hosEventDutyStatus + ", hosEventOdometer=" + hosEventOdometer
                + ", hosEventStartOdometer=" + hosEventStartOdometer + ", hosEventStartTime=" + hosEventStartTime
                + ", hosEventEndTime=" + hosEventEndTime + ", hosEventDuration=" + hosEventDuration
                + ", hosEventTetheredToDongle=" + hosEventTetheredToDongle + ", hosEventLatitude=" + hosEventLatitude
                + ", hosEventLongitude=" + hosEventLongitude + ", hosEventLocation=" + hosEventLocation
                + ", hosEventSpeed=" + hosEventSpeed + ", hosEventVin=" + hosEventVin + ", hosEventComments="
                + hosEventComments + ", hosEventRule=" + hosEventRule + ", hosEventsIs100air=" + hosEventsIs100air
                + ", hosEventsIsAdverse=" + hosEventsIsAdverse + ", hosEventsExemptionId=" + hosEventsExemptionId
                + ", hoseEventsSubstatusId=" + hoseEventsSubstatusId + ", hosEventsTripName=" + hosEventsTripName
                + ", hosEventsDrivingStartThreshold=" + hosEventsDrivingStartThreshold
                + ", hosEventsDrivingEndThreshold=" + hosEventsDrivingEndThreshold + ", hosEventDOT=" + hosEventDOTNumber
                + ", hosEventELog=" + hosEventELog + ", status=" + status + "]";
    }

}
