package usercreation;


public class ApiHoSEventV1 {

	private long endTimeStamp;

	private String dutyStatus;

	private int eventId;

	private long odometer;

	private long startOdometer;

	private double duration;

	private short tetheredToDongle;

	private double latitude;

	private double longitude;

	private String vin;

	private String comment;

	private String locationName;

	private int speed;

	private  String tractorNumber;

	private  String trailerNumber;

	private RuleSetInfo ruleSetInfo;
	String tripName;

	private int dSThreshold;

	private int dEThreshold;
	
	private short isEdited;
	
	private String dot;
	
	private short eLog;
	
	public long getEndTimeStamp() {
		return endTimeStamp;
	}

	public void setEndTimeStamp(long endTimeStamp) {
		this.endTimeStamp = endTimeStamp;
	}

	public String getDutyStatus() {
		return dutyStatus;
	}

	public void setDutyStatus(String dutyStatus) {
		this.dutyStatus = dutyStatus;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public long getOdometer() {
		return odometer;
	}

	public void setOdometer(long odometer) {
		this.odometer = odometer;
	}

	public double getDuration() {
		return duration;
	}

	public void setDuration(double duration) {
		this.duration = duration;
	}

	public short getTetheredToDongle() {
		return tetheredToDongle;
	}

	public void setTetheredToDongle(short tetheredToDongle) {
		this.tetheredToDongle = tetheredToDongle;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public String getTractorNumber() {
		return tractorNumber;
	}

	public void setTractorNumber(String tractorNumber) {
		this.tractorNumber = tractorNumber;
	}

	public String getTrailerNumber() {
		return trailerNumber;
	}

	public void setTrailerNumber(String trailerNumber) {
		this.trailerNumber = trailerNumber;
	}

	public RuleSetInfo getRuleSetInfo() {
		return ruleSetInfo;
	}

	public void setRuleSetInfo(RuleSetInfo ruleSetInfo) {
		this.ruleSetInfo = ruleSetInfo;
	}
	public long getStartOdometer() {
		return startOdometer;
	}

	public void setStartOdometer(long startOdometer) {
		this.startOdometer = startOdometer;
	}

	public String getTripName() {
		return tripName;
	}

	public void setTripName(String tripName) {
		this.tripName = tripName;
	}

	public int getdSThreshold() {
		return dSThreshold;
	}

	public void setdSThreshold(int dSThreshold) {
		this.dSThreshold = dSThreshold;
	}

	public int getdEThreshold() {
		return dEThreshold;
	}

	public void setdEThreshold(int dEThreshold) {
		this.dEThreshold = dEThreshold;
	}
	
	public short getIsEdited() {
		return isEdited;
	}

	public void setIsEdited(short isEdited) {
		this.isEdited = isEdited;
	}

	public String getDot() {
		return dot;
	}

	public void setDot(String dot) {
		this.dot = dot;
	}


	public short geteLog() {
		return eLog;
	}

	public void seteLog(short eLog) {
		this.eLog = eLog;
	}

	public void createApiHoSStatusObject(HoSEvent hoSEvent) {
		this.setEndTimeStamp(hoSEvent.getHosEventEndTime());
		this.setDutyStatus(hoSEvent.getHosEventDutyStatus());
		this.setEventId(hoSEvent.getHosEventCode());
		this.setOdometer(hoSEvent.getHosEventOdometer());
		this.setStartOdometer(hoSEvent.getHosEventStartOdometer());
		this.setDuration(hoSEvent.getHosEventDuration());
		if (hoSEvent.isHosEventTetheredToDongle()) {
			this.setTetheredToDongle((short) 1);
		} else {
			this.setTetheredToDongle((short) 0);
		}
		this.setLatitude(hoSEvent.getHosEventLatitude());
		this.setLongitude(hoSEvent.getHosEventLongitude());
		this.setVin(hoSEvent.getHosEventVin());
		this.setComment(hoSEvent.getHosEventComments());
		this.setLocationName(hoSEvent.getHosEventLocation());
		this.setSpeed(hoSEvent.getHosEventSpeed());
		this.setTractorNumber(hoSEvent.getHosEventTractorNumber());
		this.setTrailerNumber(hoSEvent.getHosEventTrailerNumber());
		RuleSetInfo ruleSetInfo=new RuleSetInfo();
		ruleSetInfo.setExemptionId(hoSEvent.getHosEventsExemptionId());
		ruleSetInfo.setIs100AirMile(hoSEvent.getHosEventsIs100air());
		ruleSetInfo.setIsAdverse(hoSEvent.getHosEventsIsAdverse());
		ruleSetInfo.setSubStatusType(hoSEvent.getHoseEventsSubstatusId());
		ruleSetInfo.setUserRuleSet(hoSEvent.getHosEventRule());
		this.setRuleSetInfo(ruleSetInfo);
		this.setTripName(hoSEvent.getHosEventsTripName());
		//changed here
		this.setdSThreshold(hoSEvent.getHosEventsDrivingStartThreshold());
		this.setdEThreshold(hoSEvent.getHosEventsDrivingEndThreshold());
		this.setDot(hoSEvent.getHosEventDOTNumber());
		this.seteLog(hoSEvent.getHosEventELog());

	}

/*	public void createApiStatusEditObject(EditHoSEvent editHoSEvent) {
		this.setEndTimeStamp(editHoSEvent.getEditHosEventEndTime());
		this.setDutyStatus(editHoSEvent.getEditHosEventDutyStatus());
		this.setEventId(editHoSEvent.getEditHosEventCode());
		this.setOdometer(editHoSEvent.getEditHosEventOdometer());
		this.setStartOdometer(editHoSEvent.getEditHosEventStartOdometer());
		this.setDuration(editHoSEvent.getEditHosEventDuration());
		if (editHoSEvent.isEditHosEventTetheredToDongle()) {
			this.setTetheredToDongle((short) 1);
		} else {
			this.setTetheredToDongle((short) 0);
		}
		this.setLatitude(editHoSEvent.getEditHosEventLatitude());
		this.setLongitude(editHoSEvent.getEditHosEventLongitude());
		this.setVin(editHoSEvent.getEditHosEventVin());
		this.setComment(editHoSEvent.getEditHosEventComments());
		this.setLocationName(editHoSEvent.getEditHosEventLocation());
		this.setSpeed(editHoSEvent.getEditHosEventSpeed());
		this.setTractorNumber(editHoSEvent.getEditHosEventTractorNumber());
		this.setTrailerNumber(editHoSEvent.getEditHosEventTrailerNumber());
		RuleSetInfo ruleSetInfo=new RuleSetInfo();
		ruleSetInfo.setExemptionId(editHoSEvent.getEditHosEventsExemptionId());
		ruleSetInfo.setIs100AirMile(editHoSEvent.getEditHosEventsIs100air());
		ruleSetInfo.setIsAdverse(editHoSEvent.getEditHosEventsIsAdverse());
		ruleSetInfo.setSubStatusType(editHoSEvent.getEditHosEventsSubstatusId());
		ruleSetInfo.setUserRuleSet(editHoSEvent.getEditHosEventRule());
		this.setRuleSetInfo(ruleSetInfo);
		this.setTripName(editHoSEvent.getEditHosEventsTripName());
		//changed here
		this.setdSThreshold(editHoSEvent.getEditHosEventsDrivingStartThreshold());
		this.setdEThreshold(editHoSEvent.getEditHosEventsDrivingEndThreshold());
		this.setDot(editHoSEvent.getEditHosEventDoTNumber());
		//
		if (editHoSEvent.getEditHosEventsIsEditAffected()) {
			this.setIsEdited((short) 1);
		} else {
			this.setIsEdited((short) 0);
		}
	}*/

	@Override
	public String toString() {
		return "ApiHoSEventV1 [endTs=" + endTimeStamp + ", status=" + dutyStatus + ", eventId=" + eventId
                        + ", odo=" + odometer + ", sOdo=" + startOdometer + ", dur=" + duration
                        + ", tether=" + tetheredToDongle + ", lat=" + latitude + ", lon=" + longitude
                        + ", location=" + locationName + ", tractorNumber=" + tractorNumber 
                        + ", isEdited=" + isEdited + ", dot=" + dot + ", eLog=" + eLog + "]";
	}



}
