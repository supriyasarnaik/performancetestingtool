/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usercreation;

/**
 *
 * @author ssarnaik
 */
public class UserLoginRes {
    
   


    private LoginUserDetailsResponse userDetails;

    private String subscriptionStatus;

    public LoginUserDetailsResponse getUserDetails() {
        return userDetails;
    }

    public String getSubscriptionStatus() {
        return subscriptionStatus;
    }

    public void setUserDetails(LoginUserDetailsResponse userDetails) {
        this.userDetails = userDetails;
    }

    public void setSubscriptionStatus(String subscriptionStatus) {
        this.subscriptionStatus = subscriptionStatus;
    }

   
}
    
    
    
   
